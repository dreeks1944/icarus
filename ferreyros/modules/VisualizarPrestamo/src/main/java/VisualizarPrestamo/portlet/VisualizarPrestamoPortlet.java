package VisualizarPrestamo.portlet;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;

import VisualizarPrestamo.constants.VisualizarPrestamoPortletKeys;
import VisualizarPrestamo.dto.PrestamosDTO;

/**
 * @author esilva
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Inteligo",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Visualizar Prestamo Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=VisualizarPrestamoPortlet",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class VisualizarPrestamoPortlet extends MVCPortlet {
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		final String REST_BASE_URI = VisualizarPrestamoPortletKeys.UrlRestPrestamos;
		
		System.out.println("REST_BASE_URI " + REST_BASE_URI);
		RestTemplate restTemplate = new RestTemplate();
		
		ThemeDisplay  themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY); 
		
		String idclie = //"100344";
				(String) themeDisplay.getUser().getExpandoBridge().getAttribute("ID_USER_INTELIGO");
		System.out.println("idclie "+idclie);
		
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("idclie", idclie);
		
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		
		ResponseEntity<String> responsePrestamos = restTemplate.exchange(REST_BASE_URI,HttpMethod.POST, new HttpEntity<String>(headers), String.class);
		String mockupPrestamos = responsePrestamos.getBody();
		
		Gson gson = new Gson();
		PrestamosDTO prestamosDTO = gson.fromJson(mockupPrestamos, PrestamosDTO.class);
		
		renderRequest.setAttribute("prestamosDTO", prestamosDTO);
		renderRequest.setAttribute("idclie", idclie);
		
		super.render(renderRequest, renderResponse);
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)throws IOException, PortletException{
		
		String cmd = ParamUtil.getString(resourceRequest, "cmd");
		String idporta = ParamUtil.getString(resourceRequest,"idportafolio");
		
		if(cmd.equals("getrentahistorica")){
			
			resourceResponse.getWriter().print(rentHistoricas(StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, idporta));
			
		}
	}
	
	private String rentHistoricas(String idclie, String fecini, String fecpro, String idporta){
		
		final String REST_BASE_URI = "http://localhost:8090/RESTlocal/renthistorica";
		
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("idclie", idclie);
		map.add("fecini", fecini);
		map.add("fecpro", fecpro);
		map.add("idporta", idporta);
		
		String rentHistoricaParentDTO = restTemplate.postForObject(REST_BASE_URI, map, String.class);
		
		return rentHistoricaParentDTO;
	}
}