package VisualizarPrestamo.dto;

public class PrestamoDTO {

	private String numcta;
	private String tippre;
	private String salcta;
	private String id;

	public PrestamoDTO(String numcta, String tippre, String salcta, String id) {
		super();
		this.numcta = numcta;
		this.tippre = tippre;
		this.salcta = salcta;
		this.id = id;
	}

	public PrestamoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNumcta() {
		return numcta;
	}
	public void setNumcta(String numcta) {
		this.numcta = numcta;
	}
	public String getTippre() {
		return tippre;
	}
	public void setTippre(String tippre) {
		this.tippre = tippre;
	}
	public String getSalcta() {
		return salcta;
	}
	public void setSalcta(String salcta) {
		this.salcta = salcta;
	}
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "PrestamoDTO [numcta=" + numcta + ", tippre=" + tippre + ", salcta=" + salcta + ", id=" + id + "]";
	}

	
	
}