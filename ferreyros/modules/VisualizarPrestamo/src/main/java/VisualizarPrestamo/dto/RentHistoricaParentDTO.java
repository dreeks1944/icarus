package VisualizarPrestamo.dto;

import java.util.List;

public class RentHistoricaParentDTO {

	private List<RentHistoricaDTO> renthist;

	public RentHistoricaParentDTO(List<RentHistoricaDTO> renthist) {
		super();
		this.renthist = renthist;
	}

	public RentHistoricaParentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "RentHistoricaParent []";
	}

	public List<RentHistoricaDTO> getRenthist() {
		return renthist;
	}

	public void setRenthist(List<RentHistoricaDTO> renthist) {
		this.renthist = renthist;
	}
	
	
	
}
