package VisualizarPrestamo.dto;

public class RentHistoricaDTO {

	private String anho;
	private String rentax;
	private String valmer;
	private String flag;
	private String tope;
	
	public String getAnho() {
		return anho;
	}
	public void setAnho(String anho) {
		this.anho = anho;
	}
	public String getRentax() {
		return rentax;
	}
	public void setRentax(String rentax) {
		this.rentax = rentax;
	}
	public String getValmer() {
		return valmer;
	}
	public void setValmer(String valmer) {
		this.valmer = valmer;
	}
	public String getTope() {
		return tope;
	}
	public void setTope(String tope) {
		this.tope = tope;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public RentHistoricaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public RentHistoricaDTO(String anho, String rentax, String valmer, String flag, String tope) {
		super();
		this.anho = anho;
		this.rentax = rentax;
		this.valmer = valmer;
		this.flag = flag;
		this.tope = tope;
	}
	
	@Override
	public String toString() {
		return "RentHistoricaDTO [anho=" + anho + ", rentax=" + rentax + ", valmer=" + valmer + ", flag=" + flag
				+ ", tope=" + tope + "]";
	}
	
	
}
