package VisualizarPrestamo.dto;

import java.util.List;

public class PrestamosDTO {

	private String totpres;
	private List<PrestamoDTO> prestamoDto;
	
	public PrestamosDTO(String totpres, List<PrestamoDTO> prestamoDto) {
		super();
		this.totpres = totpres;
		this.prestamoDto = prestamoDto;
	}
	
	public PrestamosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTotpres() {
		return totpres;
	}
	public void setTotpres(String totpres) {
		this.totpres = totpres;
	}
	public List<PrestamoDTO> getPrestamoDto() {
		return prestamoDto;
	}
	public void setPrestamoDto(List<PrestamoDTO> prestamoDto) {
		this.prestamoDto = prestamoDto;
	}
	
	@Override
	public String toString() {
		return "PrestamosDTO [totpres=" + totpres + ", prestamoDto=" + prestamoDto + "]";
	}
	
}