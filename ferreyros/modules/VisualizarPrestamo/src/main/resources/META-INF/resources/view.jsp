<%@ include file="/init.jsp" %>
	
<div id="prestamos">
   <div class="card">
     <div class="card-header" id="headerPrestamos" data-toggle="collapse" data-target="#collapsePrestamos" 
     	aria-expanded="true" aria-controls="collapseOne">
       Préstamos
       <span id="prestamos999" name="${prestamosDTO.totpres}" class="float-right formatoNumero">${prestamosDTO.totpres}</span>
     </div>
     <c:if test = "${prestamosDTO.totpres ne '0' }">
	     <div id="collapsePrestamos" class="collapse"  data-parent="#prestamos">
	       <div class="card-body">
	         <ul class="list-group">
				<c:forEach items="${prestamosDTO.prestamoDto}" var="p" varStatus="loop">
					<li class="list-group-item btndetalleprestamo" data-nroprestamo="${p.numcta}" data-idprestamo="${p.id}" data-tipoprestamo="${p.tippre}" data-nrocliente="${idclie}">
						${p.tippre} ${p.numcta}
			             <div class="float-right">
			               <span id="prestamo${loop.index}" class="formatoNumero" name="${p.salcta}">
								${p.salcta}
							</span>
			             </div>
					</li>
				</c:forEach>
	         </ul>
	       </div>
	     </div>
	   </c:if>  
   </div>
</div>

<div class="modal fade modalRentabilidadHistorica" id="modalRentabilidadHistorica" role="dialog" style="display: none;">
   <div class="modal-dialog">
     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <div class="container-fluid">
           <div class="row">
             <div class="col-md-11 col-sm-11 col-xs-11">
                 <p><strong>Portafolio <span class="pnlidportafolio"></span></strong></p>
             </div>
             <div class="col-md-1 col-sm-1 col-xs-1 align-self-center text-center">
             	<button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
           </div>
         </div>
       </div>
       <div class="modal-body">
         <div class="container-fluid">
           <div class="row">
             <div class="col-md-12 mt-4">
                 <div class="content-table-rentHistorica">
                   <table id="tablerRentabilidadHistorica" class="fixed_headers">
                     <thead>
                       <tr>
                         <th>Fecha</th>
                         <th>Valor de mercado (USD)</th>
                         <th>Rentabilidad Anual</th>
                       </tr>
                     </thead>
                     <tbody class="pnlrentahistorica">
                     </tbody>
                   </table>
                 </div>
             </div>
           </div>
         </div>
       </div>
    </div>
   </div>
  </div>
  

<portlet:resourceURL var="getrentahistorica" />
  
<script type="text/javascript">

function tooltipValue(value){

	var resultTooltip = "";
	
	if(value.includes("+")){
		resultTooltip = "-success";
	}else if(value.includes("-")){
		resultTooltip = "-danger";
	}
	
	return resultTooltip;
}

$(document).ready(function(){

    $("body").on("click",'.btnrentanual', function () {
		
    	var idportafolio = $(this).attr('data-portafolio');
		$('.pnlidportafolio').html(idportafolio);
    	
    	$.ajax({
			method: "POST",
			url : '<%=getrentahistorica%>',
			data : {<%=renderResponse.getNamespace()%>cmd: 'getrentahistorica', 
				<%=renderResponse.getNamespace()%>idportafolio: idportafolio
			},
			dataType: "json",
			success : function(data) {
				
				var htmltr = '';
				
				$.each(data.renthist, function(index, value) {
					
					htmltr += '<tr>';
					htmltr += '<td>'+value.anho+'</td>';
					htmltr += '<td>'+value.valmer+'</td>';
                    htmltr += '<td class="text'+tooltipValue(value.rentax)+'">'+value.rentax+'%</td>';
                    htmltr += '</tr>';
					
			    });
				
				$('.pnlrentahistorica').html(htmltr);
			}
		});
	});
	
});
</script>