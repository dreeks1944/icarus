package com.inteligo.consuming.services.model;

import java.util.List;

public class IbaResult {
	
	String ibaguid;
	String returncode;
	String returndesc;
	List<Row> listRow;
	
	List<Row> lstactivosProductoBancario;
	List<Row> lstactivosInversiones;
	List<Row> lstoperacionesCredito;
	
	
	
	public List<Row> getLstactivosProductoBancario() {
		return lstactivosProductoBancario;
	}
	public void setLstactivosProductoBancario(List<Row> lstactivosProductoBancario) {
		this.lstactivosProductoBancario = lstactivosProductoBancario;
	}
	public List<Row> getLstactivosInversiones() {
		return lstactivosInversiones;
	}
	public void setLstactivosInversiones(List<Row> lstactivosInversiones) {
		this.lstactivosInversiones = lstactivosInversiones;
	}
	public List<Row> getLstoperacionesCredito() {
		return lstoperacionesCredito;
	}
	public void setLstoperacionesCredito(List<Row> lstoperacionesCredito) {
		this.lstoperacionesCredito = lstoperacionesCredito;
	}
	public String getIbaguid() {
		return ibaguid;
	}
	public void setIbaguid(String ibaguid) {
		this.ibaguid = ibaguid;
	}
	public String getReturncode() {
		return returncode;
	}
	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}
	public String getReturndesc() {
		return returndesc;
	}
	public void setReturndesc(String returndesc) {
		this.returndesc = returndesc;
	}
	public List<Row> getListRow() {
		return listRow;
	}
	public void setListRow(List<Row> listRow) {
		this.listRow = listRow;
	}
	
	
	
}
