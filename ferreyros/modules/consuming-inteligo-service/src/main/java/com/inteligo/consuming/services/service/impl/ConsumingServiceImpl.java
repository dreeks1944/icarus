package com.inteligo.consuming.services.service.impl;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.rpc.ServiceException;

import org.osgi.service.component.annotations.Component;

import com.inteligo.consuming.services.model.IbaQuery;
import com.inteligo.consuming.services.service.ConsumingService;

import mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_ServiceLocator;

@Component(immediate = true, property = {
		// TODO enter required service properties
}, service = ConsumingService.class)

public class ConsumingServiceImpl implements ConsumingService {
	
	public String callServiceConverter(IbaQuery ibaguid) throws   IOException {
		String xmlString = ObjectToXML(ibaguid);
		Converter_ServiceLocator servicio = new Converter_ServiceLocator();
		String resultado = "";
		try {
			resultado = servicio.getConverterPort().traduceFileXmlToOfs(xmlString, "local");
		System.err.println("###########Calling Servicio method##################"  );
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	public String ObjectToXML(IbaQuery query) {
		String xmlString = "";
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(IbaQuery.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(query, sw);
			xmlString = sw.toString();

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return xmlString;
	}
}
