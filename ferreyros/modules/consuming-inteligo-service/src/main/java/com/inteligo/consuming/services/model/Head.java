package com.inteligo.consuming.services.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"ibaguid", "functioncode", "channel","agentId","customerNo"})
//propOrder={"company", "scheme", "agreementNumber"})
public class Head {

	
	String ibaguid;
	String functioncode;
	String channel;
	String agentId;
	String customerNo;
	
	
	public String getIbaguid() {
		return ibaguid;
	}
	
	@XmlElement
	public void setIbaguid(String ibaguid) {
		this.ibaguid = ibaguid;
	}
	public String getFunctioncode() {
		return functioncode;
	}
	
	@XmlElement
	public void setFunctioncode(String functioncode) {
		this.functioncode = functioncode;
	}
	public String getChannel() {
		return channel;
	}
	
	@XmlElement
	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAgentId() {
		return agentId;
	}
	
	@XmlElement(name="AgentId")
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getCustomerNo() {
		return customerNo;
	}
	
	@XmlElement(name="CustomerNo")
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	
}
