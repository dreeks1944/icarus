package com.inteligo.consuming.services.model;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="IbaQuery")
@XmlType(propOrder={"head","body"})
public class IbaQuery {

	Head head;
	Body body;
	
	public Head getHead() {
		return head;
	}
	
	@XmlElement
	public void setHead(Head head) {
		this.head = head;
	}

	public Body getBody() {
		return body;
	}

	@XmlElement
	public void setBody(Body body) {
		this.body = body;
	}
	
	
	
}