package com.inteligo.consuming.services.service;

import java.io.IOException;

import com.inteligo.consuming.services.model.IbaQuery;

public interface ConsumingService {
	
	public String callServiceConverter(IbaQuery ibaguid) throws IOException ;
	public String ObjectToXML(IbaQuery query) ;
}
