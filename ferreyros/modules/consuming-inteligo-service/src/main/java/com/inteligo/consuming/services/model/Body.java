package com.inteligo.consuming.services.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={})
public class Body {
	
	String customerNo;
	String accountNo;
	String portfolioNo;
	
	//Datos de transferencia
	String transactionType;
	String accountFrom;
	String debitCcy;
	String debitAmt;
	String transDate;
	String payer;
	String accountTo;
	String creditCcy;
	String valueDate;
	String ftId;
	
	
	
	public String getPortfolioNo() {
		return portfolioNo;
	}
	@XmlElement(name="PortfolioNo")
	public void setPortfolioNo(String portfolioNo) {
		this.portfolioNo = portfolioNo;
	}

	public String getAccountNo() {
		return accountNo;
	}

	@XmlElement(name="AccountNo")
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}
	
	@XmlElement(name="CustomerNo")
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	//Datos de Transferencia
	public String getTransactionType() {
		return transactionType;
	}
	
	@XmlElement(name="TransactionType")
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getAccountFrom() {
		return accountFrom;
	}
	
	@XmlElement(name="AccountFrom")
	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}
	public String getDebitCcy() {
		return debitCcy;
	}
	
	@XmlElement(name="DebitCcy")
	public void setDebitCcy(String debitCcy) {
		this.debitCcy = debitCcy;
	}
	public String getDebitAmt() {
		return debitAmt;
	}
	
	@XmlElement(name="DebitAmt")
	public void setDebitAmt(String debitAmt) {
		this.debitAmt = debitAmt;
	}
	public String getTransDate() {
		return transDate;
	}
	
	@XmlElement(name="TransDate")
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getPayer() {
		return payer;
	}
	
	@XmlElement(name="Payer")
	public void setPayer(String payer) {
		this.payer = payer;
	}
	public String getAccountTo() {
		return accountTo;
	}
	
	@XmlElement(name="AccountTo")
	public void setAccountTo(String accountTo) {
		this.accountTo = accountTo;
	}
	public String getCreditCcy() {
		return creditCcy;
	}
	
	@XmlElement(name="CreditCcy")
	public void setCreditCcy(String creditCcy) {
		this.creditCcy = creditCcy;
	}
	public String getValueDate() {
		return valueDate;
	}
	
	@XmlElement(name="ValueDate")
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	
	@XmlElement(name="FTId")
	public void setFtId(String ftId) {
		this.ftId = ftId;
	}
	public String getFtId() {
		return ftId;
	}
}
