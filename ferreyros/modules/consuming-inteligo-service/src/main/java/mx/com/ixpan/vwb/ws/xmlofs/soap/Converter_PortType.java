/**
 * Converter_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.ixpan.vwb.ws.xmlofs.soap;

public interface Converter_PortType extends java.rmi.Remote {
    public java.lang.String traduceXmlToOfs(java.lang.String xmlSerialized, java.lang.String environmentName) throws java.rmi.RemoteException;
    public java.lang.String traduceFileXmlToOfs(java.lang.String xmlSerialized, java.lang.String environmentName) throws java.rmi.RemoteException;
}
