/**
 * Converter_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.ixpan.vwb.ws.xmlofs.soap;

public interface Converter_Service extends javax.xml.rpc.Service {
    public java.lang.String getConverterPortAddress();

    public mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_PortType getConverterPort() throws javax.xml.rpc.ServiceException;

    public mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_PortType getConverterPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
