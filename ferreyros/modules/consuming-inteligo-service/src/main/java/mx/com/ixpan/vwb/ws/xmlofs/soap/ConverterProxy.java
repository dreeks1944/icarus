package mx.com.ixpan.vwb.ws.xmlofs.soap;

public class ConverterProxy implements mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_PortType {
  private String _endpoint = null;
  private mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_PortType converter_PortType = null;
  
  public ConverterProxy() {
    _initConverterProxy();
  }
  
  public ConverterProxy(String endpoint) {
    _endpoint = endpoint;
    _initConverterProxy();
  }
  
  private void _initConverterProxy() {
    try {
      converter_PortType = (new mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_ServiceLocator()).getConverterPort();
      if (converter_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)converter_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)converter_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (converter_PortType != null)
      ((javax.xml.rpc.Stub)converter_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public mx.com.ixpan.vwb.ws.xmlofs.soap.Converter_PortType getConverter_PortType() {
    if (converter_PortType == null)
      _initConverterProxy();
    return converter_PortType;
  }
  
  public java.lang.String traduceXmlToOfs(java.lang.String xmlSerialized, java.lang.String environmentName) throws java.rmi.RemoteException{
    if (converter_PortType == null)
      _initConverterProxy();
    return converter_PortType.traduceXmlToOfs(xmlSerialized, environmentName);
  }
  
  public java.lang.String traduceFileXmlToOfs(java.lang.String xmlSerialized, java.lang.String environmentName) throws java.rmi.RemoteException{
    if (converter_PortType == null)
      _initConverterProxy();
    return converter_PortType.traduceFileXmlToOfs(xmlSerialized, environmentName);
  }
  
  
}