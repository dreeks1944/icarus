<%@ include file="/init.jsp" %>

<div class="content_info">
	<!--INICIO DE SESI�N-->
	<div class="content_formulario">
		<div class="content_tabs_login">
			<a href="javascript:void(0);" class="tabs_login tabs_login_active">Clave</a>
			<a href="javascript:void(0);" class="tabs_login">Token</a>
		</div>
		
		<c:choose>
			<c:when test="<%= themeDisplay.isSignedIn() %>">
		
				<%
				
				String signedInAs = HtmlUtil.escape(user.getFullName());
		
				if (themeDisplay.isShowMyAccountIcon() && (themeDisplay.getURLMyAccount() != null)) {
					String myAccountURL = String.valueOf(themeDisplay.getURLMyAccount());
		
					signedInAs = "<a class=\"signed-in\" href=\"" + HtmlUtil.escape(myAccountURL) + "\">" + signedInAs + "</a>";
				}
				%>
		
				<liferay-ui:message arguments="<%= signedInAs %>" key="you-are-signed-in-as-x" translateArguments="<%= false %>" />
			</c:when>
			<c:otherwise>
			
				<%
				String redirect = ParamUtil.getString(request, "redirect");
				
				LiferayPortletURL forgetpasswordURL = PortletURLFactoryUtil.create(request, "com_liferay_login_web_portlet_LoginPortlet", themeDisplay.getLayout(), "RENDER_PHASE");
				forgetpasswordURL.setPortletMode(new PortletMode("view"));
				forgetpasswordURL.setWindowState(new WindowState("maximized"));
				forgetpasswordURL.setParameter("mvcRenderCommandName", "/login/forgot_password");
				%>
				
				<portlet:actionURL name="/login/login" var="loginURL">
					<portlet:param name="mvcRenderCommandName" value="/login/login" />
				</portlet:actionURL>
				
				<aui:form action="<%= loginURL %>" autocomplete='on' cssClass="sign-in-form" method="post" name="loginForm">
				
					<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
					<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
					
					<div class="inline-alert-container lfr-alert-container"></div><br/>
				
					<liferay-util:dynamic-include key="com.liferay.login.web#/login.jsp#alertPre" />
				
					<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
					<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-log-in-because-the-maximum-number-of-users-has-been-reached" />
					<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
					<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
					<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
					<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeNull.class %>" message="please-enter-an-email-address" />
					<liferay-ui:error exception="<%= UserLockoutException.LDAPLockout.class %>" message="this-account-is-locked" />
	
					<liferay-ui:error exception="<%= UserLockoutException.PasswordPolicyLockout.class %>">
	
						<%
						UserLockoutException.PasswordPolicyLockout ule = (UserLockoutException.PasswordPolicyLockout)errorException;
						%>
	
						<c:choose>
							<c:when test="<%= ule.passwordPolicy.isRequireUnlock() %>">
								<liferay-ui:message key="this-account-is-locked" />
							</c:when>
							<c:otherwise>
								<liferay-ui:message arguments="<%= ule.user.getUnlockDate() %>" key="this-account-is-locked-until-x" translateArguments="<%= false %>" />
							</c:otherwise>
						</c:choose>
					</liferay-ui:error>
					
					<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
					<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNull.class %>" message="the-screen-name-cannot-be-blank" />
	
					<liferay-util:dynamic-include key="com.liferay.login.web#/login.jsp#alertPost" />
				
					<div class="items_campos">
						<aui:input type="text"  autoFocus="true" label="Usuario" cssClass="clearable input_login" 
							name="login" showRequiredLabel="<%= false %>" value="">
							<aui:validator name="required" errorMessage="El usuario es requerido" />
						</aui:input>
					</div>
		
					<div class="items_campos">
						<aui:input name="password" cssClass="input_login" label="Clave" showRequiredLabel="<%= false %>" type="password">
							<aui:validator name="required" errorMessage="La clave es requerida" />
						</aui:input>
					</div>
					
					<a href="<%= forgetpasswordURL %>" class="btn_olvidar">Olvid� mi clave</a>
					<aui:button-row>
						<aui:button cssClass="btn-lg btn_ingresar_portal" type="submit" value="sign-in" />
					</aui:button-row>
				</aui:form>
				
			</c:otherwise>
		</c:choose>
		
	</div>
	<!--FIN INICIO DE SESI�N-->
	<%
		JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(themeDisplay.getScopeGroupId(), "contenido-web-login");
	%>
	<liferay-journal:journal-article articleId="<%=journalArticle.getArticleId() %>" groupId="<%= themeDisplay.getScopeGroupId() %>" />
	<!--FIN-->
</div>
<a href="#" class="btn_necesita">Necesito otra cosa</a>

<script type="text/javascript">
var referrer = "<%=themeDisplay.getCDNBaseURL()%>" + '/c/portal/logout';
if(document.referrer == referrer){
	location.reload(true);
}
</script>
