package com.example.login.portlet;

import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

@Component(
	property = {
		"javax.portlet.name=MyLoginPortlet",
		"mvc.command.name=/login/login"
	},
	service = MVCActionCommand.class
)
public class MyLoginMVCActionCommand extends BaseMVCActionCommand {

	private static final String ADMINISTRATOR = "Administrator";
	private static final String ASESOR_COMERCIAL = "Asesor Comercial";
	private static final String URL_ASESOR = "/web/guest/bienvenido-asesor";

	@Override
	protected void doProcessAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception{

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);
		
		HttpServletRequest request = PortalUtil.getOriginalServletRequest(
			PortalUtil.getHttpServletRequest(actionRequest));
		
		HttpServletResponse response = PortalUtil.getHttpServletResponse(
			actionResponse);

		String login = ParamUtil.getString(actionRequest, "login");
		String password = actionRequest.getParameter("password");
		boolean rememberMe = ParamUtil.getBoolean(actionRequest, "rememberMe");
		String authType = null;

		AuthenticatedSessionManagerUtil.login(
			request, response, login, password, rememberMe, authType);
		
		long userId = AuthenticatedSessionManagerUtil.getAuthenticatedUserId(request, login, password, authType);
		
		List<Role> buddyTempListRole = RoleLocalServiceUtil.getUserRoles(userId);
		
		String urlReturn = themeDisplay.getPathMain();
		
		for (Role role : buddyTempListRole) {
			if(ASESOR_COMERCIAL.equals(role.getName()) || role.getName().equals(ADMINISTRATOR)){
				urlReturn = URL_ASESOR;
				break;
			}else{
				urlReturn = themeDisplay.getPathMain();
			}
		}
		
		actionResponse.sendRedirect(urlReturn);
		
	}

}
