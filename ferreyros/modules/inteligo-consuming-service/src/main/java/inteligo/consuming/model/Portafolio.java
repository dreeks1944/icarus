
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Portafolio {
    private String owner;
    private String numpor;
    private String rentabi;
    private Double totpor;
    private List<DtPortafolio> data = new LinkedList<DtPortafolio>();
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
	public List<DtPortafolio> getData() {
		return data;
	}
	public void setData(List<DtPortafolio> data) {
		this.data = data;
	}
	
    
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getNumpor() {
		return numpor;
	}
	public void setNumpor(String numpor) {
		this.numpor = numpor;
	}
	
	public String getRentabi() {
		return rentabi;
	}
	public void setRentabi(String rentabi) {
		this.rentabi = rentabi;
	}
	public Double getTotpor() {
		return totpor;
	}
	public void setTotpor(Double totpor) {
		this.totpor = totpor;
	}
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


    
    
}
