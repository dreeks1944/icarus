
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.Map;

public class DtCash {

    private String estitu;
    private String ctaaso;
    private String numcta;
    private Double salcta;
    private String tipcta;
    private String estcta;
    
    
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getEstitu() {
        return estitu;
    }

    public void setEstitu(String estitu) {
        this.estitu = estitu;
    }

    public String getCtaaso() {
        return ctaaso;
    }

    public void setCtaaso(String ctaaso) {
        this.ctaaso = ctaaso;
    }

    public String getNumcta() {
        return numcta;
    }

    public void setNumcta(String numcta) {
        this.numcta = numcta;
    }

    public Double getSalcta() {
        return salcta;
    }

    public void setSalcta(Double salcta) {
        this.salcta = salcta;
    }

    public String getTipcta() {
        return tipcta;
    }

    public void setTipcta(String tipcta) {
        this.tipcta = tipcta;
    }

    public String getEstcta() {
        return estcta;
    }

    public void setEstcta(String estcta) {
        this.estcta = estcta;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
