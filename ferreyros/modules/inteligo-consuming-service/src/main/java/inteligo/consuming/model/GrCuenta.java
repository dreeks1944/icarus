
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.Map;

public class GrCuenta {

    private String nomcta;
    private Double totcta;
    private Portafolio portafolio;
    private Depositos depositos;
    private Cash cash;
    private String estcta;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getNomcta() {
        return nomcta;
    }

    public void setNomcta(String nomcta) {
        this.nomcta = nomcta;
    }

    public Double getTotcta() {
        return totcta;
    }

    public void setTotcta(Double totcta) {
        this.totcta = totcta;
    }

    public Portafolio getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(Portafolio portafolio) {
        this.portafolio = portafolio;
    }

    public Depositos getDepositos() {
        return depositos;
    }

    public void setDepositos(Depositos depositos) {
        this.depositos = depositos;
    }

    public Cash getCash() {
        return cash;
    }

    public void setCash(Cash cash) {
        this.cash = cash;
    }

    public String getEstcta() {
        return estcta;
    }

    public void setEstcta(String estcta) {
        this.estcta = estcta;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
