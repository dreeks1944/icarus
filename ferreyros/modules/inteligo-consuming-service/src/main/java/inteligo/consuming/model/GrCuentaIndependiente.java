
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GrCuentaIndependiente {

    private List<DtCuentasIndependiente> dtCuentasIndependientes = new LinkedList<DtCuentasIndependiente>();
    private Double tocuen;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<DtCuentasIndependiente> getDtCuentasIndependientes() {
        return dtCuentasIndependientes;
    }

    public void setDtCuentasIndependientes(List<DtCuentasIndependiente> dtCuentasIndependientes) {
        this.dtCuentasIndependientes = dtCuentasIndependientes;
    }

    public Double getTocuen() {
        return tocuen;
    }

    public void setTocuen(Double tocuen) {
        this.tocuen = tocuen;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
