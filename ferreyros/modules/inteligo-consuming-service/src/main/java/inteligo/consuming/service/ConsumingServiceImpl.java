package inteligo.consuming.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.ProtocolException;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.StringPool;

@Component(immediate = true, property = {
		// TODO enter required service properties
}, service = ConsumingService.class)

public class ConsumingServiceImpl implements ConsumingService {	

	private final static Logger log = Logger.getLogger(ConsumingServiceImpl.class.getSimpleName());
	
	public JSONObject invokeRestApiByGet(String url, Map<String, String> headers, LinkedHashMap<String, String> parameters) {
		JSONObject json = null;
		try {
			
		    StringBuilder urlService = new StringBuilder(url);

		    Set<Entry<String, String>> valoresParametros = parameters.entrySet();
		    //For get-params
		    for (Entry<String, String> entry : valoresParametros) {
		    	urlService.append(StringPool.SLASH).append(entry.getValue());
		    }
		    
		    //Valid URL
		    URL urlCall = new URL(urlService.toString());
		    HttpURLConnection conn = (HttpURLConnection) urlCall.openConnection();

		    Set<Entry<String, String>> valoresHeader = headers.entrySet();
		    for (Entry<String, String> entry : valoresHeader) {
		    	conn.addRequestProperty(entry.getKey(), entry.getValue());
		    }
		    conn.setRequestMethod("GET");
		    conn.setRequestProperty("Accept", "application/json");
		    if (conn.getResponseCode() != HttpStatus.SC_OK) {
				json = null;
				log.error("Failed : HTTP error code : " + conn.getResponseCode());
		    } else {
		    	json = JSONFactoryUtil.createJSONObject(IOUtils.toString(conn.getInputStream()));
		    }

		    conn.disconnect();
		  //System.out.println("Invoke URL:" + urlService.toString());
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
		    json = null;
		    log.error(e);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
		    json = null;
		    log.error(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		    json = null;
		    log.error(e);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		    json = null;
		    log.error(e);
		}

		return json;
	}

	public String invokeRestApiByPost(String url, String request, Map<String, String> headers, LinkedHashMap<String, String> parameters) {
	    	String json = null;

	    	final HttpClient client = new HttpClient();

	    	final PostMethod post = new PostMethod(url);

	    	if (request != null && !request.isEmpty()) {
	    	    RequestEntity requestEntity = new InputStreamRequestEntity(IOUtils.toInputStream(request),"application/json");
	    	    post.setRequestEntity(requestEntity);
	    	}
	    	
	    	Set<Entry<String, String>> valoresParametros = parameters.entrySet();
	    	
	    	for (Entry<String, String> entry : valoresParametros) {
	    		post.setParameter(entry.getKey(), entry.getValue());
	    	}
	    	
	    	Set<Entry<String, String>> valoresHeaders = headers.entrySet();
	    	for (Entry<String, String> entry : valoresHeaders) {
	    		post.addRequestHeader(entry.getKey(), entry.getValue());
	    	}

	    	try {
	    	    client.executeMethod(post);

	    	    json =IOUtils.toString( post.getResponseBodyAsStream());

	    	    switch (post.getStatusCode()) {
		    	    case 200: {
	
		    		break;
		    	    }

		    	    default:
//			    		log.debug("Invalid response code (" + post.getStatusCode() + ") from CAS server!");
//			    		log.debug("Response (1k): " + json.substring(0, Math.min(1024, json.length())));
		    		break;
	    	    }
	    	} catch (final IOException e) {
	    	    log.error(e.getMessage());
	    	} finally {
	    	    post.releaseConnection();
	    	}

	    	return json;
	        }

}
