
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Depositos {

    private Double totdep;
    private List<DtDeposito> data  = new LinkedList<DtDeposito>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
	public Double getTotdep() {
		return totdep;
	}
	public void setTotdep(Double totdep) {
		this.totdep = totdep;
	}
	public List<DtDeposito> getData() {
		return data;
	}
	public void setData(List<DtDeposito> data) {
		this.data = data;
	}
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
    

    
    
}
