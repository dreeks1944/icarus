
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.Map;

public class DtCuentasIndependiente {

    private String numcta;
    private Double salcta;
    private String tipcta;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getNumcta() {
        return numcta;
    }

    public void setNumcta(String numcta) {
        this.numcta = numcta;
    }

    public Double getSalcta() {
        return salcta;
    }

    public void setSalcta(Double salcta) {
        this.salcta = salcta;
    }

    public String getTipcta() {
        return tipcta;
    }

    public void setTipcta(String tipcta) {
        this.tipcta = tipcta;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
