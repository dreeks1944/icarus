
package inteligo.consuming.model;

public class DtPortafolio {

	private String tipgru;
    private String descrip;
    private Integer saldo;
    private String tipoprod;
    private String codpro;
    private String tipoclas;
    
    
	public String getTipgru() {
		return tipgru;
	}
	public void setTipgru(String tipgru) {
		this.tipgru = tipgru;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public Integer getSaldo() {
		return saldo;
	}
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	public String getTipoprod() {
		return tipoprod;
	}
	public void setTipoprod(String tipoprod) {
		this.tipoprod = tipoprod;
	}
	public String getCodpro() {
		return codpro;
	}
	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}
	public String getTipoclas() {
		return tipoclas;
	}
	public void setTipoclas(String tipoclas) {
		this.tipoclas = tipoclas;
	}
    
    
    
    
}
