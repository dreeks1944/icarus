
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.Map;

public class DtDeposito {

    private Double saldep;
    private String tipdep;
    private String coddep;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Double getSaldep() {
        return saldep;
    }

    public void setSaldep(Double saldep) {
        this.saldep = saldep;
    }

    public String getTipdep() {
        return tipdep;
    }

    public void setTipdep(String tipdep) {
        this.tipdep = tipdep;
    }

    public String getCoddep() {
        return coddep;
    }

    public void setCoddep(String coddep) {
        this.coddep = coddep;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
