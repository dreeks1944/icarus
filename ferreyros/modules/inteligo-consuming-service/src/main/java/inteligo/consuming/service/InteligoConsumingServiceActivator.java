package inteligo.consuming.service;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * @author user
 */
public class InteligoConsumingServiceActivator implements BundleActivator {

	 private ServiceRegistration registration;
     
	    @Override
	    public void start(BundleContext context) throws Exception {
	 
	        registration = context.registerService(ConsumingService.class.getName(), new ConsumingServiceImpl(), null);
	        System.out.println("###########Service Registered Successfully##############");
	    }
	 
	    @Override
	    public void stop(BundleContext context) throws Exception {
	        registration.unregister();
	        System.out.println("###########Service Unregistered##############");
	         
	    }


}