
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PatrimonioInitial {

    private List<Patrimonio> patrimonio = new LinkedList<Patrimonio>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



	public List<Patrimonio> getPatrimonio() {
		return patrimonio;
	}



	public void setPatrimonio(List<Patrimonio> patrimonio) {
		this.patrimonio = patrimonio;
	}



	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}



	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

   

}
