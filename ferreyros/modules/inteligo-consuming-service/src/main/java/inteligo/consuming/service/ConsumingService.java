package inteligo.consuming.service;

import java.util.LinkedHashMap;
import java.util.Map;

import com.liferay.portal.kernel.json.JSONObject;

public interface ConsumingService {
	
	
	public JSONObject invokeRestApiByGet(String url, Map<String, String> headers, LinkedHashMap<String, String> parameters);
		
	public String invokeRestApiByPost(String url, String request, Map<String, String> headers, LinkedHashMap<String, String> parameters);

}
