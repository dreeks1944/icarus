
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Cash {

    private Double tocash;
    private List<DtCash> data = new LinkedList<DtCash>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
	public Double getTocash() {
		return tocash;
	}
	public void setTocash(Double tocash) {
		this.tocash = tocash;
	}
	
	
	
	
	public List<DtCash> getData() {
		return data;
	}
	public void setData(List<DtCash> data) {
		this.data = data;
	}
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

   

}
