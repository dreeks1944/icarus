
package inteligo.consuming.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Patrimonio {

    private List<GrCuentaIndependiente> grupocuentasindependientes = new LinkedList<GrCuentaIndependiente>();
    private List<GrCuenta> grupocuentas= new LinkedList<GrCuenta>();
    private Double baltot;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

   

    public List<GrCuentaIndependiente> getGrupocuentasindependientes() {
		return grupocuentasindependientes;
	}

	public void setGrupocuentasindependientes(List<GrCuentaIndependiente> grupocuentasindependientes) {
		this.grupocuentasindependientes = grupocuentasindependientes;
	}

	public List<GrCuenta> getGrupocuentas() {
		return grupocuentas;
	}

	public void setGrupocuentas(List<GrCuenta> grupocuentas) {
		this.grupocuentas = grupocuentas;
	}

	public Double getBaltot() {
		return baltot;
	}

	public void setBaltot(Double baltot) {
		this.baltot = baltot;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
