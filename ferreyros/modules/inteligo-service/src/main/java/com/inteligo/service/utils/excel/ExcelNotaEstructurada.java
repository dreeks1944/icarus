package com.inteligo.service.utils.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


public class ExcelNotaEstructurada {

	public static HSSFWorkbook hssfWorkbook;
	
	public static void setHSSFWorkbook(OutputStream outputStream) throws IOException{
		hssfWorkbook.write(outputStream);
	}
	
	
	public static void generarHSSFWorkbook(
			LinkedHashMap<String, Object[]> dataTitulo, 
			LinkedHashMap<String, Object[]> dataCabecera){
		
		// Finds the workbook instance for XLSX file 
		hssfWorkbook = ExcelUtilitario.createHSSFWorkbook();
		// Return first sheet from the XLSX workbook 
		HSSFSheet mySheet = hssfWorkbook.createSheet("Nota Estructurada");
		
		
		
        HSSFCellStyle styleTitulo = ExcelUtilitario.obtenerEstiloTitulo(hssfWorkbook);
        ExcelUtilitario.generarExcelTitulo(hssfWorkbook, mySheet, styleTitulo, dataTitulo,0,false);
        ExcelUtilitario.generarFilaVacia(hssfWorkbook, mySheet, 1);        
        ExcelUtilitario.generarFilaVacia(hssfWorkbook, mySheet, 2);
        
        int numeroFilaInicioCabeceraDetalle= 3;
        
        HSSFCellStyle styleCabecera = ExcelUtilitario.obtenerEstiloCabecera(hssfWorkbook);
        HSSFCellStyle styleValor = ExcelUtilitario.obtenerEstiloValor(hssfWorkbook);
        ExcelUtilitario.generarExcelCabecera(hssfWorkbook, mySheet, styleCabecera, styleValor, dataCabecera,numeroFilaInicioCabeceraDetalle,true);
        
        
		
	}

	
}
