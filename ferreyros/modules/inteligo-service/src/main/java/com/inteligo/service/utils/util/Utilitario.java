package com.inteligo.service.utils.util;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utilitario {

	public static String obtenerDescripcionMoneda(String acronimoMoneda){
		String descripcionMoneda = "";
		if (acronimoMoneda.equals("USD")){
			descripcionMoneda = "D�lares";
		}
		if (acronimoMoneda.equals("EUR")){
			descripcionMoneda = "Euros";
		}
		if (acronimoMoneda.equals("PEN")){
			descripcionMoneda = "Soles";
		}
		return descripcionMoneda;
	}
	
	
	public static String obtenerSimboloMoneda(String acronimoMoneda){
		String descripcionMoneda = "";
		if (acronimoMoneda.equals("USD")){
			descripcionMoneda = "$";
		}
		if (acronimoMoneda.equals("EUR")){
			descripcionMoneda = "�";
		}
		if (acronimoMoneda.equals("PEN")){
			descripcionMoneda = "S/";
		}
		return descripcionMoneda;
	}
	
	public static String obtenerMontoFormato(BigDecimal numero){
		try {
			DecimalFormatSymbols simbolos = DecimalFormatSymbols.getInstance(Locale.ENGLISH);
			DecimalFormat formateador = new DecimalFormat("#,##0.00",simbolos);
			String numeroFormato = formateador.format(numero);
			return numeroFormato;
		} catch (Exception e) {
			e.printStackTrace();
			return "0.00";
		}
	}
	
	public static String obtenerFechaFormato(String fechayyyymmdd){
		Locale espanol = new Locale("es","ES");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String fecha = fechayyyymmdd;
        try {
            Date date = formatter.parse(fecha);
			SimpleDateFormat format = new SimpleDateFormat("EEE, dd MM yyyy",espanol);
			String fechaNuevoFormato = format.format(date);
			return fechaNuevoFormato;
        } 
		catch (Exception e) {
            return "";
        }
	}
	
	public static String obtenerFechaPersonalizadoFormato(String fecha, String patternFormat){
		Locale espanol = new Locale("es","ES");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        try {
            Date date = formatter.parse(fecha);
			SimpleDateFormat format = new SimpleDateFormat(patternFormat,espanol);
			String fechaNuevoFormato = format.format(date);
			return fechaNuevoFormato;
        } 
		catch (Exception e) {
            return "";
        }
	}
	
	public static BigDecimal obtenerParticipacionPorcentaje(String numeroPorcion, String numeroTotal){
		return new BigDecimal(Double.parseDouble(numeroPorcion) * 100 / Double.parseDouble(numeroTotal)).setScale(2, RoundingMode.HALF_UP);
	}

	public static String getCurrentDateFileBuilder(String productType){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		Date date = new Date();
		
		if(Validator.isNotNull(productType)){
			productType = productType.concat(StringPool.UNDERLINE).concat(sdf.format(date));
		}else{
			productType = sdf.format(date);
		}
		
		return productType;
		
	}

	
	
}
