package com.inteligo.service.utils.excel;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

@SuppressWarnings("deprecation")
public class ExcelUtilitario {

	
	public static HSSFWorkbook createHSSFWorkbook (){
		HSSFWorkbook myWorkBook = new HSSFWorkbook (); 
		return myWorkBook;
	}
	
	
	
	public static HSSFCell createCell(HSSFWorkbook HSSFWorkbook, HSSFRow row, Integer numberColumn, Object valueCell){
		HSSFCell cell = row.createCell(numberColumn);
		
		if (valueCell instanceof String) {
            cell.setCellValue((String) valueCell);
        } else if (valueCell instanceof Boolean) {
            cell.setCellValue((Boolean) valueCell);
        } else if (valueCell instanceof Date) {
            cell.setCellValue((Date) valueCell);
        } else if (valueCell instanceof Double) {
            cell.setCellValue((Double) valueCell);
        }
        else if (valueCell instanceof Integer) {
            cell.setCellValue((Integer) valueCell);
        }
        else if (valueCell instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) valueCell).doubleValue());
        }
		
        return cell;
        
	}
	
	public static HSSFCellStyle obtenerEstiloTitulo(HSSFWorkbook HSSFWorkbook){
		
		
		HSSFFont font = HSSFWorkbook.createFont();
		font.setFontHeightInPoints((short) 22);
		font.setFontName("ARIAL");
		font.setItalic(false);
		font.setBold(true);
        font.setColor(HSSFColor.BLACK.index);
        
        
        HSSFCellStyle style = HSSFWorkbook.createCellStyle();
        style.setFont(font);
	        
        return style;    
	}
	
	public static HSSFCellStyle obtenerEstiloCabecera(HSSFWorkbook HSSFWorkbook){
		
		
		HSSFFont font = HSSFWorkbook.createFont();
		font.setFontHeightInPoints((short) 13);
		font.setFontName("ARIAL");
		font.setItalic(false);
		font.setBold(true);
        font.setColor(HSSFColor.BLACK.index);
        
        
        HSSFCellStyle style = HSSFWorkbook.createCellStyle();
        style.setFont(font);
	        
        return style;    
	}
	
	public static HSSFCellStyle obtenerEstiloValor(HSSFWorkbook HSSFWorkbook){
		
		
		HSSFFont font = HSSFWorkbook.createFont();
		font.setFontHeightInPoints((short) 13);
		font.setFontName("ARIAL");
		font.setItalic(false);
        font.setColor(HSSFColor.BLACK.index);
        
        
        HSSFCellStyle style = HSSFWorkbook.createCellStyle();
        style.setFont(font);
	        
        return style;    
	}
	
	
	public static HSSFCellStyle obtenerEstiloHeaderDetalle(HSSFWorkbook HSSFWorkbook){
		
		
		HSSFFont font = HSSFWorkbook.createFont();
		font.setFontHeightInPoints((short) 10);
		font.setFontName("ARIAL");
        font.setColor(HSSFColor.WHITE.index);
        
        
        HSSFCellStyle backgroundStyle = HSSFWorkbook.createCellStyle();

        backgroundStyle.setFillBackgroundColor(new HSSFColor.BLUE().getIndex());
        backgroundStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
        backgroundStyle.setFillForegroundColor(new HSSFColor.BLUE().getIndex());
        backgroundStyle.setFont(font);

        backgroundStyle.setBorderBottom(CellStyle.BORDER_THIN);
        backgroundStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        backgroundStyle.setBorderLeft(CellStyle.BORDER_THIN);
        backgroundStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        backgroundStyle.setBorderRight(CellStyle.BORDER_THIN);
        backgroundStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        backgroundStyle.setBorderTop(CellStyle.BORDER_THIN);
        backgroundStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        
        
        
        return backgroundStyle;
        
        
	}
	
	public static HSSFCellStyle obtenerEstiloDetalle(HSSFWorkbook HSSFWorkbook){
		
		
		HSSFFont font = HSSFWorkbook.createFont();
		font.setFontHeightInPoints((short) 10);
		font.setFontName("ARIAL");
        font.setColor(HSSFColor.BLACK.index);
        
        HSSFCellStyle style = HSSFWorkbook.createCellStyle();
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setFont(font);
        
        return style;
        
        
	}
	
	
	public static void generarExcel(HSSFWorkbook HSSFWorkbook, HSSFSheet mySheet, HSSFCellStyle style, LinkedHashMap<String, Object[]> data, int filaInicio, boolean autosizeColumn){
		// Set to Iterate and add rows into XLS file
        Set<String> newRows = data.keySet();
        
        // get the last row number to append new data          
        int rownum = filaInicio;         
     
        for (String key : newRows) {
         
            // Creating a new Row in existing XLSX sheet
            HSSFRow row = mySheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
            	HSSFCell cell = ExcelUtilitario.createCell(HSSFWorkbook, row, cellnum, obj);
            	cell.setCellStyle(style);
            	if(autosizeColumn){
            		int columnIndex = cell.getColumnIndex();
            		mySheet.autoSizeColumn(columnIndex);
            	}
                cellnum++;
            }
        }
	}
	
	public static void generarExcelCabecera(HSSFWorkbook HSSFWorkbook, HSSFSheet mySheet, HSSFCellStyle styleTitulo, HSSFCellStyle styleValor, LinkedHashMap<String, Object[]> data, int filaInicio, boolean autosizeColumn){
		// Set to Iterate and add rows into XLS file
        Set<String> newRows = data.keySet();
        
        // get the last row number to append new data          
        int rownum = filaInicio;         
     
        for (String key : newRows) {
        	
        	
         
            // Creating a new Row in existing XLSX sheet
            HSSFRow row = mySheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
            	HSSFCell cell = ExcelUtilitario.createCell(HSSFWorkbook, row, cellnum, obj);
            	
            	if (Integer.parseInt(key)%2==0){
            		cell.setCellStyle(styleValor);
            	}
            	else{
            		cell.setCellStyle(styleTitulo);
            	}
            	
            	if(autosizeColumn){
            		int columnIndex = cell.getColumnIndex();
            		mySheet.autoSizeColumn(columnIndex);
            	}
                cellnum++;
            }
        }
	}
	
	
	public static void generarFilaVacia(HSSFWorkbook HSSFWorkbook, HSSFSheet mySheet, int filaInicio){
		// get the last row number to append new data          
        int rownum = filaInicio;    
        
        mySheet.createRow(rownum++);
     
        
	}
	
	public static void generarExcelDetalle(HSSFWorkbook HSSFWorkbook, HSSFSheet mySheet, HSSFCellStyle styleCabecera, HSSFCellStyle styleDetalle, LinkedHashMap<String, Object[]> data, int filaInicio, boolean autosizeColumn){
		// Set to Iterate and add rows into XLS file
        Set<String> newRows = data.keySet();
        
        // get the last row number to append new data          
        int rownum = filaInicio;         
     
        for (String key : newRows) {
         
            // Creating a new Row in existing XLSX sheet
            HSSFRow row = mySheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
            	HSSFCell cell = ExcelUtilitario.createCell(HSSFWorkbook, row, cellnum, obj);
            	
            	if (Integer.parseInt(key)==1){
            		cell.setCellStyle(styleCabecera);
            	}
            	else{
            		cell.setCellStyle(styleDetalle);
            	}
            	
            	if(autosizeColumn){
            		int columnIndex = cell.getColumnIndex();
            		mySheet.autoSizeColumn(columnIndex);
            	}
                cellnum++;
            }
        }
	}
	
	
	public static void generarExcelTitulo(HSSFWorkbook HSSFWorkbook, HSSFSheet mySheet, HSSFCellStyle style, LinkedHashMap<String, Object[]> data, int filaInicio, boolean autosizeColumn){
		// Set to Iterate and add rows into XLS file
        Set<String> newRows = data.keySet();
        
        // get the last row number to append new data          
        int rownum = filaInicio;         
     
        for (String key : newRows) {
         
            // Creating a new Row in existing XLSX sheet
            HSSFRow row = mySheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
            	
            	
            	HSSFCell cell = ExcelUtilitario.createCell(HSSFWorkbook, row, cellnum, obj);
            	cell.setCellStyle(style);
            	
                mySheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 10));
            	
            	if(autosizeColumn){
            		int columnIndex = cell.getColumnIndex();
            		mySheet.autoSizeColumn(columnIndex);
            	}
                cellnum++;
            }
        }
	}

	
	
	
	
}
