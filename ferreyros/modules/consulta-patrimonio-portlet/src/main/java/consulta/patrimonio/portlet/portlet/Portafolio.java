package consulta.patrimonio.portlet.portlet;

public class Portafolio {
	
	private String group;
	private String producttype;
	private String assetclass;
	private String sector;
	private String region;
	private String currency;
	private String securityid;
	private String productname;
	private double currentbalance;
	private String realdist;
	private int indiceColor;
	

	
	public double getCurrentbalance() {
		return currentbalance;
	}
	public void setCurrentbalance(double currentbalance) {
		this.currentbalance = currentbalance;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getProducttype() {
		return producttype;
	}
	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}
	public String getAssetclass() {
		return assetclass;
	}
	public void setAssetclass(String assetclass) {
		this.assetclass = assetclass;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSecurityid() {
		return securityid;
	}
	public void setSecurityid(String securityid) {
		this.securityid = securityid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getRealdist() {
		return realdist;
	}
	public void setRealdist(String realdist) {
		this.realdist = realdist;
	}
	public int getIndiceColor() {
		return indiceColor;
	}
	public void setIndiceColor(int indiceColor) {
		this.indiceColor = indiceColor;
	}
	
	
	
	
}
