package consulta.patrimonio.portlet.portlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.liferay.portal.kernel.util.StringPool;

public class UtilitiesFormat {
	
	private static final Logger log = Logger.getLogger(UtilitiesFormat.class.getSimpleName());
	/**
	 * Formatea los datos de tipo BigInteger en "200.000,00"
	 * 
	 * @param var
	 * @return
	 */
	
	//"dd/MM/yyyy"
	public static String formatDate(Date fecha, String formato) {
		String fechaFormateada = StringPool.BLANK;
		if (fecha != null){
			SimpleDateFormat formatter = new SimpleDateFormat(formato);
			fechaFormateada = formatter.format(fecha);
		}
		return fechaFormateada;
	}
	public static Date formateString(String fecha, String formato){
		
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		Date fechaConvertida = new Date(); 
		try {
			fechaConvertida = formatter.parse(fecha);
		} catch (ParseException e) {
			System.out.println("Error convirtiendo la fecha ::"+e);
		}
		return fechaConvertida;
	}
	
	public static String formatDouble(double value){
		return String.format("$%,.2f", value);
	}
	
}
