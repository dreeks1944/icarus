package consulta.patrimonio.portlet.portlet;

public class Sign {
	
	private String customerno;
	private String accountno;
	private String signrelation;
	private String signgroup;
	private String signcondition;
	private String signlink;
	
	
	public String getCustomerno() {
		return customerno;
	}
	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getSignrelation() {
		return signrelation;
	}
	public void setSignrelation(String signrelation) {
		this.signrelation = signrelation;
	}
	public String getSigngroup() {
		return signgroup;
	}
	public void setSigngroup(String signgroup) {
		this.signgroup = signgroup;
	}
	public String getSigncondition() {
		return signcondition;
	}
	public void setSigncondition(String signcondition) {
		this.signcondition = signcondition;
	}
	public String getSignlink() {
		return signlink;
	}
	public void setSignlink(String signlink) {
		this.signlink = signlink;
	}
	

	
}
