package consulta.patrimonio.portlet.portlet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.inteligo.consuming.services.model.Body;
import com.inteligo.consuming.services.model.Head;
import com.inteligo.consuming.services.model.IbaQuery;
import com.inteligo.consuming.services.model.IbaResult;
import com.inteligo.consuming.services.model.Row;
import com.inteligo.consuming.services.service.ConsumingService;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import consulta.patrimonio.portlet.constants.ConsultaPatrimonioPortletKeys;

/**
 * @author user
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=consulta-patrimonio-portlet Portlet",
//		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.header-portlet-javascript=/js/canvasjs.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/Chart.js",
		"com.liferay.portlet.header-portlet-javascript=/js/init.js",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ConsultaPatrimonioPortletKeys.ConsultaPatrimonio,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ConsultaPatrimonioPortlet extends MVCPortlet {

	@Reference
	public void set__consumingService(ConsumingService _consumingService) {
		this._consumingService = _consumingService;
	}
	
	private ConsumingService _consumingService;
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		
//		UserNotificationEventLocalServiceUtil userNotificationEventLocalServiceUtil = 
//		UserNotificationEventLocalServiceUtil.addUserNotificationEvent(userId, type, timestamp, deliverBy, payload, archived, serviceContext)
		
		// TODO Auto-generated method stub
		Locale locale = new Locale("es");
		DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(',');
		String pattern = "#,##0.00";
		//Get USER_ID_INTELIGO
		
		ThemeDisplay  themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY); 
		
		String idUserInteligo = //"100344";
				(String) themeDisplay.getUser().getExpandoBridge().getAttribute("ID_USER_INTELIGO");
		
		System.out.println("####USER INTELIGO #####"+idUserInteligo);
		
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		IbaQuery ibaguid10 = new IbaQuery();
		Head hd10 = new Head();
		Body bd10 = new Body();		
		hd10.setIbaguid(generateIbaGuid(idUserInteligo));
		hd10.setFunctioncode("10");
		hd10.setChannel("BZG");
		hd10.setAgentId("FC-10-TESTC1");
		hd10.setCustomerNo(idUserInteligo);
		ibaguid10.setHead(hd10);
		ibaguid10.setBody(bd10);
		
//		Map<Patrimonio, > mapperPatrimonio = new TreeMap<>();
		Map<String, Number > mapperPasivo = new TreeMap<>();
		Map<String, Number > mapperActivo = new TreeMap<>();
		Map<String, Number > mapperInversiones = new TreeMap<>();
		Map<String, List<Account>> mapperCuentas = new TreeMap<>();
		Map<String, String> mapperCuentasSum = new TreeMap<>();
		Map<String, String> mapperInversionesSum = new TreeMap<>();

		
		Map<String, Map<String, List<Portafolio>>> mapperPortafolio = new TreeMap<String, Map<String, List<Portafolio>>>();
		
		if (_consumingService != null) {
			String strFC10_return = _consumingService.callServiceConverter(ibaguid10);
			try {

			Document dc10 = SAXReaderUtil.read(strFC10_return.toString());
			Element rootE10 = dc10.getRootElement();
			Element body10 = rootE10.element("body");
			Element lstElement10 = body10.element("tables").element("table");
			List<Element> lst10table =  lstElement10.elements();
			String[] isPasivo = { "Loans","Guarantee Issued","Credit Cards"};
			String[] isActivo = { "Investments", "Bank Products"};
//			"Investments", "Bank Products"
//			PASIVOS
//			Prestamos
//			Garantias
//			Tarjeta de credito
			
			Number sumPatrimonio = 0;
			Number sumPasivo = 0;
			Number cashSum = 0;
			Number activoSum = 0;
			for (Element element : lst10table) {
				Row rw = new Row();
				rw.setDescription((String) element.element("Description").getData());
				rw.setReference((String) element.element("Reference").getData());
				rw.setBalance((String) element.element("Balance").getData());
				rw.setGroup((String) element.element("Group").getData());
				
				
				if(ArrayUtil.contains(isPasivo,rw.getDescription())){
					if(rw.getDescription().equals("Loans")){
						rw.setDescription("Prestamos");
					}else if(rw.getDescription().equals("Guarantee Issued")){
						rw.setDescription("Garantia emitida");
					}else if(rw.getDescription().equals("Credit Cards")){
						rw.setDescription("Tarjetas de credito");
					}
					
					mapperPasivo.put(rw.getDescription(), NumberFormat.getNumberInstance(Locale.US).parse(rw.getBalance().toString().trim()));
				}else {
					//if(ArrayUtil.contains(isActivo,rw.getGroup())){
					//agrupar cash
					if(rw.getDescription().equals("Current Account") || rw.getDescription().equals("Tradding Account")){
						//Si ingreso , ir al function code 01 para poder llamar a todas las  cuentas tradding - current
						
						
						
						
						IbaQuery ibaguid01 = new IbaQuery();
						Head hd01 = new Head();
						Body bd01 = new Body();
						hd01.setIbaguid(generateIbaGuid(idUserInteligo));
						hd01.setFunctioncode("1");
						hd01.setChannel("BZG");
						hd01.setCustomerNo(idUserInteligo);
						ibaguid01.setHead(hd01);
						bd01.setCustomerNo(idUserInteligo);
						ibaguid01.setBody(bd01);
						String strFC01_return = _consumingService.callServiceConverter(ibaguid01);
						Document dc01 = SAXReaderUtil.read(strFC01_return.toString());
						Element rootE01 = dc01.getRootElement();
						Element body01 = rootE01.element("body");
						Element lstElement01 = body01.element("tables");
						List<Element> lst01table =  lstElement01.elements();
						
						for (Element element01 : lst01table) {
							List<Element> tablee = element01.elements();						
							if (element01.attributeValue("name").equalsIgnoreCase("AC.LIST")) {
								for (Element element1 : tablee) {
									String accCustomer = (String) element1.element("AccCustomer").getData();
									if(accCustomer.equals(idUserInteligo)){
										Account acc = new Account();
										acc.setAccounttype((String) element1.element("AccountType").getData());
										acc.setAccountno((String) element1.element("AccountNo").getData());
										acc.setAccountassoc((String) element1.element("AccountAssoc").getData());
										acc.setCollateralactive(acc.getAccounttype().concat(StringPool.COLON.concat(StringPool.SPACE)).concat((String) element1.element("AccountAssoc").getData()));
										acc.setAcccustomer((String) element1.element("AccCustomer").getData());
										
										String str = element1.element("WorkingBal").getData().toString().trim();
										if(str != ""){
											acc.setWorkingbal(NumberFormat.getNumberInstance(Locale.US).parse(str).doubleValue());
										}else{
											acc.setWorkingbal(0);
										}
										if(acc.getAccounttype().equals("Current Account")){
											acc.setAccounttype("Cuenta Corriente");
										}else if(acc.getAccounttype().equals("Trading Account")){
											acc.setAccounttype("Cuenta Trading");
										}
										
										List<Account> ListAccount = mapperCuentas.get(acc.getAccounttype());
										if (ListAccount == null) {
											ListAccount = new ArrayList<>();
											 mapperCuentas.put(acc.getAccounttype(), ListAccount);
										}
										ListAccount.add(acc);
									}
								}
							}
						}
					}else if(!rw.getDescription().equals("Deposits")){//agrupar inversiones 
						//System.out.println("###Inversiones  mapperInversiones" + rw.getBalance());
						 
						IbaQuery ibaguid14 = new IbaQuery();
						Head hd14 = new Head();
						Body bd14 = new Body();
						hd14.setIbaguid(generateIbaGuid(idUserInteligo));
						hd14.setFunctioncode("12");
						hd14.setChannel("BZG");
						hd14.setAgentId("FC-12-TESTC1");
						hd14.setCustomerNo(idUserInteligo);
						ibaguid14.setHead(hd14);
						bd14.setCustomerNo(idUserInteligo);
						bd14.setPortfolioNo(rw.getReference());
						ibaguid14.setBody(bd14);
						String strFC14_return = _consumingService.callServiceConverter(ibaguid14);
						Document dc14 = SAXReaderUtil.read(strFC14_return.toString());
						Element rootE14 = dc14.getRootElement();
						Element body14 = rootE14.element("body");
						Element lstElement14 = body14.element("tables").element("table");
						List<Element> lst14table =  lstElement14.elements();
						
						Map<String, List<Portafolio>> mapperPortafolioxActivo = new HashMap<>();
						Map<String, List<Portafolio>> mapperPortafolioxProducto = new HashMap<>();
						Map<String, List<Portafolio>> mapperPortafolioxTodo = new HashMap<>();
						
						Map<String, List<Portafolio>> mapperPortafolioxActivoGrafico = new HashMap<>();
						Map<String, List<Portafolio>> mapperPortafolioxProductoGrafico = new HashMap<>();
						Map<String, List<Portafolio>> mapperPortafolioxTodoGrafico = new HashMap<>();
						
						LinkedList<Portafolio> lstportafoliotodo = new LinkedList<>();
						
						int indicecolortodo = 0;
						Number cashResta = 0;
						for (Element element14 : lst14table) {
							String group = StringUtil.lowerCase((String) element14.element("Group").getData());
							String assetClass = StringUtil.lowerCase((String) element14.element("AssetClass").getData());
							String productType = StringUtil.lowerCase((String) element14.element("ProductType").getData());
							System.out.println(group);
							System.out.println(assetClass);
							System.out.println(productType);
							if(!group.equals("cash") && !assetClass.equals("cash") && !productType.equals("cash")){
							    Portafolio prt = new Portafolio();
							    prt.setGroup((String) element14.element("Group").getData());
							    prt.setAssetclass((String) element14.element("AssetClass").getData());
							    prt.setProducttype((String) element14.element("ProductType").getData());
							    prt.setSector((String) element14.element("Sector").getData());
							    prt.setRegion((String) element14.element("Region").getData());
							    prt.setProductname((String) element14.element("ProductName").getData());
							    
							    String str = element14.element("CurrentBalance").getData().toString().trim();
							    if(str != ""){
							    	prt.setCurrentbalance(NumberFormat.getNumberInstance(Locale.US).parse(str).doubleValue());
								}else{
									prt.setCurrentbalance(0);
								}
							    //prt.setCurrentbalance(NumberFormat.getNumberInstance(Locale.US).parse((String) element14.element("CurrentBalance").getData().toString().trim()).doubleValue());
							    prt.setRealdist((String) element14.element("RealDist").getData());	
							    prt.setIndiceColor(indicecolortodo);
							    lstportafoliotodo.add(prt);
							    indicecolortodo++;
							    
							    
							    List<Portafolio> lstportafolioactivo = mapperPortafolioxActivo.get(prt.getAssetclass());
							    if (lstportafolioactivo == null) {
							    	lstportafolioactivo = new ArrayList<>();
							    	mapperPortafolioxActivo.put(prt.getAssetclass(), lstportafolioactivo);
							    }
							    lstportafolioactivo.add(prt);
							    
							    List<Portafolio> lstportafolioproducto = mapperPortafolioxProducto.get(prt.getProducttype());
							    if (lstportafolioproducto == null) {
							    	lstportafolioproducto = new ArrayList<>();
							    	mapperPortafolioxProducto.put(prt.getProducttype(), lstportafolioproducto);
							    }
							    lstportafolioproducto.add(prt);
							}else{
								cashResta = NumberFormat.getNumberInstance(Locale.US).parse(element14.element("CurrentBalance").getData().toString().trim()).doubleValue();
							}
						}
						
						
						int indicecoloractivo = 0;
						//solo setear colores
						for (Entry<String, List<Portafolio>> entry : mapperPortafolioxActivo.entrySet()) {
							mapperPortafolioxActivoGrafico.put(entry.getKey()+"|"+indicecoloractivo, entry.getValue());
							indicecoloractivo++;
						}
						
						int indicecolorproducto = 0;
						//solo setear colores
						for (Entry<String, List<Portafolio>> entry : mapperPortafolioxProducto.entrySet()) {
							
							mapperPortafolioxProductoGrafico.put(entry.getKey()+"|"+indicecolorproducto, entry.getValue());
							indicecolorproducto++;
						}
						
						mapperPortafolioxTodo.put(rw.getReference(), lstportafoliotodo);
						mapperPortafolio.put(rw.getReference()+"|"+"CLASE_ACTIVO", mapperPortafolioxActivoGrafico);
						mapperPortafolio.put(rw.getReference()+"|"+"TIPO_DE_PRODUCTO", mapperPortafolioxProductoGrafico);
						mapperPortafolio.put(rw.getReference()+"|"+"TODOS_INVERSIONES", mapperPortafolioxTodo);
						Number totalInv = NumberFormat.getNumberInstance(Locale.US).parse(rw.getBalance().toString().trim()).doubleValue() - cashResta.doubleValue();
						mapperInversiones.put(rw.getReference(),  totalInv);
						
					}
				}
//			    Total de Patrimonio
//				Number balance = NumberFormat.getNumberInstance(Locale.US).parse(rw.getBalance().trim());
//				sumPatrimonio = balance.doubleValue() + sumPatrimonio.doubleValue();
			}
			for (Entry<String, List<Account>> entry : mapperCuentas.entrySet()) {
				double sum = entry.getValue().stream().map(Account::getWorkingbal).mapToDouble(Double::doubleValue).sum(); 
				mapperCuentasSum.put(entry.getKey(), decimalFormat.format(sum));
				cashSum = sum + cashSum.doubleValue();
			}
			Number inversionessum = mapperInversiones.entrySet().stream().mapToDouble(i -> i.getValue().doubleValue()).sum();
			
			
		   
			
		   
			Random random = new Random();
	       

	        JSONArray dataxActivo = JSONFactoryUtil.createJSONArray();
	        JSONArray dataxProducto = JSONFactoryUtil.createJSONArray();
	        JSONArray dataxTodo = JSONFactoryUtil.createJSONArray();
	        JSONArray dataColores = JSONFactoryUtil.createJSONArray();
	        
			for (Entry<String, Number> entry : mapperInversiones.entrySet()) {
				Map<String, List<Portafolio>> mapportafolioActiv = mapperPortafolio.get(entry.getKey()+"|"+"CLASE_ACTIVO");
				Map<String, List<Portafolio>> mapportafolioProduc = mapperPortafolio.get(entry.getKey()+"|"+"TIPO_DE_PRODUCTO");
				Map<String, List<Portafolio>> mapportafolioTodo = mapperPortafolio.get(entry.getKey()+"|"+"TODOS_INVERSIONES");
					for (Entry<String, List<Portafolio>> entryportafolio : mapportafolioActiv.entrySet()) { 
						String keyPortafolio = entryportafolio.getKey();
						String[] separadorkeyPortafolio = keyPortafolio.split(Pattern.quote("|"));
						mapperInversionesSum.put(entry.getKey()+"_"+entryportafolio.getKey(), decimalFormat.format(entryportafolio.getValue().stream().map(Portafolio::getCurrentbalance).mapToDouble(Double::doubleValue).sum()));
						JSONObject row = JSONFactoryUtil.createJSONObject();
				        row.put("portafolio", entry.getKey());
				        row.put("producto", separadorkeyPortafolio[0]);
				        row.put("Saldo", entryportafolio.getValue().stream().map(Portafolio::getCurrentbalance).mapToDouble(Double::doubleValue).sum());
				        row.put("color", separadorkeyPortafolio[1]);
				        dataxActivo.put(row);
					}	
					
					for (Entry<String, List<Portafolio>> entryportafolio : mapportafolioProduc.entrySet()) { 
						String keyPortafolio = entryportafolio.getKey();
						String[] separadorkeyPortafolio = keyPortafolio.split(Pattern.quote("|"));
						mapperInversionesSum.put(entry.getKey()+"_"+entryportafolio.getKey(), decimalFormat.format(entryportafolio.getValue().stream().map(Portafolio::getCurrentbalance).mapToDouble(Double::doubleValue).sum()));
						JSONObject row = JSONFactoryUtil.createJSONObject();
				        row.put("portafolio", entry.getKey());
				        row.put("producto", separadorkeyPortafolio[0]);
				        row.put("Saldo", entryportafolio.getValue().stream().map(Portafolio::getCurrentbalance).mapToDouble(Double::doubleValue).sum());
				        row.put("color", separadorkeyPortafolio[1]);
				        dataxProducto.put(row);
					}
					
					for (Entry<String, List<Portafolio>> entryportafolio : mapportafolioTodo.entrySet()) { 
						//mapperInversionesSum.put(entry.getKey()+"_"+entryportafolio.getKey(), decimalFormat.format(entryportafolio.getValue().stream().map(Portafolio::getCurrentbalance).mapToDouble(Double::doubleValue).sum()));
						
						List<Portafolio> listaPortafolio = entryportafolio.getValue();
				        for (Portafolio portafolio : listaPortafolio) {
				        	JSONObject row = JSONFactoryUtil.createJSONObject();
				        	row.put("portafolio", entry.getKey());
					        row.put("producto", portafolio.getProductname());
					        row.put("Saldo", portafolio.getCurrentbalance());
					        row.put("color", portafolio.getIndiceColor());
					        dataxTodo.put(row);
						}
					}
			}
			
			
			
	        
	        List<String> listaColores = new ArrayList<String>(
				    Arrays.asList(
				    		"rgb(9, 25, 91)",
				    		"rgb(35, 62, 153)",
				    		"rgb(122, 187, 215)",
				    		"rgb(77, 77, 79)",
				    		"rgb(0, 0, 0)",
				    		"rgb(52, 122, 190)",
				    		"rgb(80, 91, 169)",
				    		"rgb(175, 211, 229)",
				    		"rgb(167, 169, 172)",
				    		"rgb(137, 129, 111)",
				    		"rgb(186, 97, 39)",
				    		"rgb(164, 186, 224)",
				    		"rgb(205, 222, 231)",
				    		"rgb(202, 203, 205)",
				    		"rgb(194, 181, 155)",
				    		"rgb(247, 147, 30)",
				    		"rgb(184, 213, 240)",
				    		"rgb(116, 173, 178)",
				    		"rgb(220, 221, 222)",
				    		"rgb(230, 226, 213)",
				    		"rgb(233, 214, 207)",
				    		"rgb(218, 227, 231)",
				    		"rgb(172, 212, 217)",
				    		"rgb(241, 241, 242)",
				    		"rgb(239, 235, 227)",
				    		"rgb(240, 230, 188)",
				    		"rgb(169, 197, 137)",
				    		"rgb(131, 143, 94)",
				    		"rgb(255, 255, 255)",
				    		"rgb(187, 118, 101)"));
	        
	        for (int j = 0; j < 170; j++) {
	        	int nextInt = random.nextInt(256*256*256);
		        String colorCode = String.format("#%06x", nextInt);
		        listaColores.add(colorCode);
			}
	        
	        for (String colorCode : listaColores) {
				JSONObject row = JSONFactoryUtil.createJSONObject();
		        row.put("color", colorCode);
		        dataColores.put(row);
			}
	        
			
			activoSum = 	inversionessum.doubleValue() + cashSum.doubleValue();
			Number pasivoSum = mapperPasivo.entrySet().stream().mapToDouble(i -> i.getValue().doubleValue()).sum();
			sumPatrimonio = activoSum.doubleValue() - pasivoSum.doubleValue();
			renderRequest.setAttribute("patrimonio", decimalFormat.format(sumPatrimonio));
			renderRequest.setAttribute("pasivo", mapperPasivo);
			renderRequest.setAttribute("cash", mapperCuentas);
			renderRequest.setAttribute("inversiones", mapperInversiones);
			renderRequest.setAttribute("portafolio", mapperPortafolio);
			renderRequest.setAttribute("mapperCuentasSum", mapperCuentasSum);
			renderRequest.setAttribute("cashsum", decimalFormat.format(cashSum));
			renderRequest.setAttribute("inversionessum", decimalFormat.format(inversionessum));
			renderRequest.setAttribute("sumActivo", decimalFormat.format(activoSum));
			renderRequest.setAttribute("sumPasivo", decimalFormat.format(pasivoSum));
			renderRequest.setAttribute("portafoliosum", mapperInversionesSum);
			renderRequest.setAttribute("dataxActivo", dataxActivo.toJSONString());
			renderRequest.setAttribute("dataxProducto", dataxProducto.toJSONString());
			renderRequest.setAttribute("dataxTodos", dataxTodo.toJSONString());
			renderRequest.setAttribute("listaColores", listaColores);
			renderRequest.setAttribute("dataColores", dataColores.toJSONString());
			System.out.println(mapperInversionesSum);
			} catch (DocumentException | java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)throws IOException, PortletException{
		
		String cmd = ParamUtil.getString(resourceRequest, "cmd");
		String numeroCuenta = ParamUtil.getString(resourceRequest,"numeroCuenta");
		System.out.println("cmd: " + cmd);
		String customerNo = ParamUtil.getString(resourceRequest,"customerNo");
		JSONObject responseServiceFirst = JSONFactoryUtil.createJSONObject();
		
		if(cmd.equals("getcuentas")){

			//Servicio FC1
			// TODO Auto-generated method stub
			
			System.out.println("====================== customerNo:" + customerNo);
			String accountNo = ParamUtil.getString(resourceRequest,"accountNo");
			System.out.println("====================== accountNo:" + accountNo);
			String typeTransference = ParamUtil.getString(resourceRequest,"typeTransference");
			System.out.println("====================== typeTransference:" + typeTransference);
			 
			IbaQuery ibaguid = new IbaQuery();
			Head hd = new Head();
			Body bd = new Body();
			hd.setIbaguid(generateIbaGuid(customerNo, accountNo));
			hd.setFunctioncode("1");
			hd.setChannel("BZG");
			hd.setCustomerNo(customerNo);
			ibaguid.setHead(hd);
			bd.setCustomerNo(customerNo);
			ibaguid.setBody(bd);
			String strFC_return = _consumingService.callServiceConverter(ibaguid);
			Document dc;
			try {
				dc = SAXReaderUtil.read(strFC_return.toString());
				Element rootE = dc.getRootElement();
				Element body = rootE.element("body");
				Element lstElement = body.element("tables");
				List<Element> lsttable =  lstElement.elements();
				JSONArray cuentaCorrienteList = JSONFactoryUtil.createJSONArray();
				for (Element element : lsttable) {
					List<Element> tablee = element.elements();						
					if (element.attributeValue("name").equalsIgnoreCase("AC.LIST")) {
						for (Element element1 : tablee) {
							if(typeTransference.equals("1")){//Transferencia Trader
								JSONObject AccountAssoc = JSONFactoryUtil.createJSONObject();
								String responseAccountNo = (String) element1.element("AccountNo").getData();
								if(responseAccountNo != null && responseAccountNo.length() > 0 && responseAccountNo.equals(accountNo)){
									String responseAccountAssoc = (String) element1.element("AccountAssoc").getData();
									if(responseAccountAssoc != null && responseAccountAssoc.length() > 0){
										AccountAssoc.put("AccountAssoc", responseAccountAssoc);
										cuentaCorrienteList.put(AccountAssoc);
									}
								}
							}else if(typeTransference.equals("2")){
								JSONObject AccountAssoc = JSONFactoryUtil.createJSONObject();
								String responseAccCustomer = (String) element1.element("AccCustomer").getData();
								if(responseAccCustomer != null && responseAccCustomer.length() > 0 && responseAccCustomer.equals(customerNo)){
									String responseAccountAssoc = (String) element1.element("AccountAssoc").getData();
									if(responseAccountAssoc != null && responseAccountAssoc.length() > 0){
										AccountAssoc.put("AccountAssoc", responseAccountAssoc);
										cuentaCorrienteList.put(AccountAssoc);
									}
								}
							}else{
								resourceResponse.getWriter().print("Error: Tipo de transferencia no valido.");
							}
						}
						
					}
				}
				resourceResponse.getWriter().print(cuentaCorrienteList.toString());
				//System.out.println("============================ responseService:" + cuentaCorrienteList.toString());
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resourceResponse.getWriter().print("Error: Ocurrio un problema.");
				
			}
			
		}else if(cmd.equals("transferencia")){
			//Servicio FC1
			//TODO Auto-generated method stub
			
			//System.out.println("======================================== Entro a sendTransactionTransference ===================");
			
//			String customerNo = ParamUtil.getString(resourceRequest,"customerNo");
			System.out.println("======================================== customerNo:" + customerNo);
			String accountFrom = ParamUtil.getString(resourceRequest,"accountFrom");
			System.out.println("======================================== accountFrom:" + accountFrom);
			String debitCcy = ParamUtil.getString(resourceRequest,"debitCcy");
			System.out.println("======================================== debitCcy:" + debitCcy);
			String debitAmt = ParamUtil.getString(resourceRequest,"debitAmt");
			System.out.println("======================================== debitAmt:" + debitAmt);
			String payer = ParamUtil.getString(resourceRequest,"payer");
			System.out.println("======================================== payer:" + payer);
			String accountTo = ParamUtil.getString(resourceRequest,"accountTo");
			System.out.println("======================================== accountTo:" + accountTo);
			String creditCcy = ParamUtil.getString(resourceRequest,"creditCcy");
			System.out.println("======================================== creditCcy:" + creditCcy);
			 
			IbaQuery ibaguid = new IbaQuery();
			Head hd = new Head();
			Body bd = new Body();
			
			hd.setIbaguid(generateIbaGuid(customerNo, accountFrom));
			hd.setFunctioncode("3");
			hd.setChannel("BZG");
			
			hd.setCustomerNo(customerNo);//10015
			
			bd.setTransactionType("AC14");
			
			bd.setAccountFrom(accountFrom);//1000156937
			bd.setDebitCcy(debitCcy);//USD
			bd.setDebitAmt(debitAmt);//187.50
			//System.out.println("======================================== getDate() transDate:" + getDate());
			bd.setTransDate("2018-01-02");//2017-12-06
			
			bd.setPayer(payer);//"ARANA ZAPATERO VDA. DE ZAMUDIO"
			bd.setAccountTo(accountTo);//"1000152637"
			bd.setCreditCcy(creditCcy);//"USD"
			//System.out.println("======================================== getDate() valueDate:" + getDate());
			bd.setValueDate("2018-01-02");//"2017-12-06"
			
			ibaguid.setHead(hd);
			ibaguid.setBody(bd);
			
			if (_consumingService != null) {
				
				try {
					//System.out.println("============================ Antes del callServiceConverter =======================");
					String map = _consumingService.callServiceConverter(ibaguid);
					//System.out.println("============================ Despues del callServiceConverter =======================");
					Document document = SAXReaderUtil.read(map.toString());
					Element rootElement = document.getRootElement();
					
					//IbaResult result = new IbaResult();
					Element head = rootElement.element("head");
					Element _ibaguid = head.element("ibaguid");
					Element _returncode = head.element("returncode");
					Element _returndesc = head.element("returndesc");
					
					Element body = rootElement.element("body");
					Element _singlefield = body.element("singlefields").element("singlefield");
					
					System.out.println("_ibaguid:" + _ibaguid.getData().toString());
					System.out.println("_returncode:" + _returncode.getData().toString());
					System.out.println("_returndesc:" + _returndesc.getData().toString());
					System.out.println("_singlefield:" + _singlefield.elementText("FTId"));
					
					JSONObject responseService = JSONFactoryUtil.createJSONObject();
					
					if(_returncode.getData().toString().equals("0") && _singlefield.getData()!= null && _singlefield.getData().toString().length() > 0){
						IbaQuery ibaguidFC4 = new IbaQuery();
						Head hdFC4 = new Head();
						Body bdFC4 = new Body();
						
						hdFC4.setIbaguid(generateIbaGuid(customerNo, accountFrom));
						hdFC4.setFunctioncode("4");
						hdFC4.setChannel("BZG");
						hdFC4.setCustomerNo(customerNo);
						bdFC4.setFtId(_singlefield.getData().toString().trim());
						ibaguidFC4.setHead(hdFC4);
						ibaguidFC4.setBody(bdFC4);
						String mapFC4 = _consumingService.callServiceConverter(ibaguidFC4);
						Document documentFC4 = SAXReaderUtil.read(mapFC4.toString());
						Element rootElementFC4 = documentFC4.getRootElement();
						Element headFC4 = rootElementFC4.element("head");
						Element _ibaguidFC4 = headFC4.element("ibaguid");
						Element _returncodeFC4 = headFC4.element("returncode");
						Element _returndescFC4 = headFC4.element("returndesc");
						
						responseService.put("_ibaguid", _ibaguidFC4.getData().toString());
						responseService.put("_date", getDate());
						responseService.put("_hour", getHour());
						responseService.put("_returncode", _returncodeFC4.getData().toString());
						responseService.put("_returndesc", _returndescFC4.getData().toString()); 
					}else{
						
						responseService.put("_ibaguid", _ibaguid.getData().toString());
						responseService.put("_date", getDate());
						responseService.put("_hour", getHour());
						responseService.put("_returncode", _returncode.getData().toString());
						responseService.put("_returndesc", _returndesc.getData().toString());
					}
					resourceResponse.getWriter().print(responseService.toJSONString()); 
					
					//System.out.println("============================ responseService:" + responseService.toString());
					
				}catch(DocumentException | IOException e){
					e.printStackTrace();
					resourceResponse.getWriter().print("Error: OcurriÃ¯Â¿Â½ un problema.");
				}
					
			}
			
		}else  if("getMovimientos".equalsIgnoreCase(cmd))
		 { 
			 
			 //Servicio FC16
			// TODO Auto-generated method stub
				IbaQuery ibaquery = new IbaQuery();
				Head hd = new Head();
				Body bd = new Body();
				hd.setIbaguid(generateIbaGuid(customerNo, numeroCuenta));
				hd.setFunctioncode("16");
				hd.setChannel("BZG");
				hd.setAgentId("FC-16-TESTC1");
				hd.setCustomerNo(customerNo);
				ibaquery.setHead(hd);
				
				bd.setAccountNo(numeroCuenta);
				ibaquery.setBody(bd);
				
				
				
				Locale locale = new Locale("es");
				DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
				symbols.setDecimalSeparator('.');
				symbols.setGroupingSeparator(',');
				String pattern = "#,###.###";
				DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
				
				Map<Integer, Map<String, String>> mapMovimiento = new HashMap<Integer, Map<String, String>>();

				JSONArray movimientoList = JSONFactoryUtil.createJSONArray();
				
				if (_consumingService != null) {			
					try {	
							String map = _consumingService.callServiceConverter(ibaquery);
							System.out.println(map);
							Document document = SAXReaderUtil.read(map.toString());
							Element rootElement = document.getRootElement();
							IbaResult result = new IbaResult();
							Element head = rootElement.element("head");
							Element _ibaguid = head.element("ibaguid");
							Element _returncode = head.element("returncode");
							Element _returndesc = head.element("returndesc");
							result.setIbaguid((String) _ibaguid.getData());
							result.setReturncode((String) _returncode.getData());
							result.setReturndesc((String) _returndesc.getData());
							
							Element body = rootElement.element("body");
							
							if (body.hasContent()){
								Element tables = body.element("tables");
								if (tables.hasContent()){
									Element table = tables.element("table");	
									if (table.hasContent()){
										List<Element> list =  table.elements();
										int i =1;
										for (Element o : list) {
											String TransDate = (String) o.element("TransDate").getData();
											String ProcessDate = (String) o.element("ProcessDate").getData();
											String Description = (String) o.element("Description").getData();
											String Debit = (String) o.element("Debit").getData();
											String Credit = (String) o.element("Credit").getData();
											String Balance = (String) o.element("Balance").getData();
											
											
											
											
											JSONObject movimiento = JSONFactoryUtil.createJSONObject();
										       movimiento.put("id", i);
										       movimiento.put("TransDate", TransDate);
										       movimiento.put("ProcessDate", ProcessDate);
										       movimiento.put("Description", Description);
										       movimiento.put("Debit", Debit);
										       movimiento.put("Credit", Credit);
										       movimiento.put("Balance", Balance);
										       movimientoList.put(movimiento); 
											
											
											
											i++;
										}
										
									}
								}
								
								
							}
							
							
							
							
							//Agrupar por tipo de producto
							
							
						}catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			 
			 
			 
			 
			 //
			 
		       
		       resourceResponse.getWriter().print(movimientoList.toString()); 
		       System.out.println("movimientoList");
		       System.out.println(movimientoList);
		       System.out.println(movimientoList.toJSONString());
		       
		  } 
		 }else{
			System.out.println("error");
			responseServiceFirst.put("result", "error");
		}
		
//		try {
//			
////			resourceResponse.getWriter().println(responseServiceFirst.toJSONString());
//			
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
		
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	private static String getDate(){
		Calendar cal = new GregorianCalendar();
   	  	String dd = (cal.get(Calendar.DAY_OF_MONTH) < 10) ? ("0" + cal.get(Calendar.DAY_OF_MONTH)) : (Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
   	 	String mm = (cal.get(Calendar.MONTH)+1 < 10) ? ("0" + (cal.get(Calendar.MONTH)+1)) : (Integer.toString(cal.get(Calendar.MONTH)+1));
   	 	String yy = Integer.toString(cal.get(Calendar.YEAR));
   	 	return (yy+"-"+mm+"-"+dd);
	}
	
	private static String getHour(){
		Calendar cal = new GregorianCalendar();
		String hh = (cal.get(Calendar.HOUR_OF_DAY) < 10) ? ("0" + cal.get(Calendar.HOUR_OF_DAY)) : (Integer.toString(cal.get(Calendar.HOUR_OF_DAY)));
		String mi = (cal.get(Calendar.MINUTE) < 10) ? ("0" + cal.get(Calendar.MINUTE)) : (Integer.toString(cal.get(Calendar.MINUTE)));
		String ss = (cal.get(Calendar.SECOND) < 10) ? ("0" + cal.get(Calendar.SECOND)) : (Integer.toString(cal.get(Calendar.SECOND)));
		return (hh+"-"+mi+"-"+ss);
	}
	
	private static String generateIbaGuid(String customerNo, String accountFrom){
		Random ram = new Random();
		return "FC3-" + customerNo + "-" + accountFrom + "-" + getDate() + "_" + getHour() +"-TC01-" + ram.nextInt(100000);
		
	}
	
	private static String generateIbaGuid(String customerNo){
		Random ram = new Random();
		return "FC3-" + customerNo + "-" + getDate() + "_" + getHour() +"-TC01-" + ram.nextInt(100000);
		
	}
}