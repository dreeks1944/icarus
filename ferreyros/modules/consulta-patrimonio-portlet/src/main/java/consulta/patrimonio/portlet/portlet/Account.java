package consulta.patrimonio.portlet.portlet;

public class Account {
	
	private String acccustomer;
	private String accountno;
	private String accountactive;
	private String accountinactive;
	private String accounttype;
	private String agreementid;
	private String accountassoc;
	private String portfolio;
	private String currency;
	private double workingbal;
	private String acctbal;
	private String olbactive;
	private String limit;
	private String limitamount;
	private String limitamtdisp;
	private String limitexpdate;
	private String collateralactive;
	private String accsegment;
	
	
	
	public String getAccountactive() {
		return accountactive;
	}
	public void setAccountactive(String accountactive) {
		this.accountactive = accountactive;
	}
	public String getAcccustomer() {
		return acccustomer;
	}
	public void setAcccustomer(String acccustomer) {
		this.acccustomer = acccustomer;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getAccountinactive() {
		return accountinactive;
	}
	public void setAccountinactive(String accountinactive) {
		this.accountinactive = accountinactive;
	}
	public String getAccounttype() {
		return accounttype;
	}
	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}
	public String getAgreementid() {
		return agreementid;
	}
	public void setAgreementid(String agreementid) {
		this.agreementid = agreementid;
	}
	public String getAccountassoc() {
		return accountassoc;
	}
	public void setAccountassoc(String accountassoc) {
		this.accountassoc = accountassoc;
	}
	public String getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
	public double getWorkingbal() {
		return workingbal;
	}
	public void setWorkingbal(double workingbal) {
		this.workingbal = workingbal;
	}
	public String getAcctbal() {
		return acctbal;
	}
	public void setAcctbal(String acctbal) {
		this.acctbal = acctbal;
	}
	public String getOlbactive() {
		return olbactive;
	}
	public void setOlbactive(String olbactive) {
		this.olbactive = olbactive;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getLimitamount() {
		return limitamount;
	}
	public void setLimitamount(String limitamount) {
		this.limitamount = limitamount;
	}
	public String getLimitamtdisp() {
		return limitamtdisp;
	}
	public void setLimitamtdisp(String limitamtdisp) {
		this.limitamtdisp = limitamtdisp;
	}
	public String getLimitexpdate() {
		return limitexpdate;
	}
	public void setLimitexpdate(String limitexpdate) {
		this.limitexpdate = limitexpdate;
	}
	public String getCollateralactive() {
		return collateralactive;
	}
	public void setCollateralactive(String collateralactive) {
		this.collateralactive = collateralactive;
	}
	public String getAccsegment() {
		return accsegment;
	}
	public void setAccsegment(String accsegment) {
		this.accsegment = accsegment;
	}
	@Override
	public String toString() {
		return "Account [acccustomer=" + acccustomer + ", accountno=" + accountno + ", accountactive=" + accountactive
				+ ", accountinactive=" + accountinactive + ", accounttype=" + accounttype + ", agreementid="
				+ agreementid + ", accountassoc=" + accountassoc + ", portfolio=" + portfolio + ", currency=" + currency
				+ ", workingbal=" + workingbal + ", acctbal=" + acctbal + ", olbactive=" + olbactive + ", limit="
				+ limit + ", limitamount=" + limitamount + ", limitamtdisp=" + limitamtdisp + ", limitexpdate="
				+ limitexpdate + ", collateralactive=" + collateralactive + ", accsegment=" + accsegment + "]";
	}
	
	
}
