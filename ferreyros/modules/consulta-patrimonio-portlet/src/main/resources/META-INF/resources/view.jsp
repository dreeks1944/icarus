<%@page import="java.util.regex.Pattern"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="consulta.patrimonio.portlet.portlet.UtilitiesFormat"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.util.Locale"%>
<%@page import="consulta.patrimonio.portlet.portlet.Account"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="java.util.List"%>

<%@page import="consulta.patrimonio.portlet.portlet.Portafolio"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@ include file="/init.jsp" %>

<portlet:resourceURL var="sendData" />

<%
String patrimonio_sum = (String) request.getAttribute("patrimonio");
String dataxActivo = (String) request.getAttribute("dataxActivo");
String dataxProducto = (String) request.getAttribute("dataxProducto");
String dataxTodos = (String) request.getAttribute("dataxTodos");
String cash_sum = (String) request.getAttribute("cashsum");
String inversiones_sum = (String) request.getAttribute("inversionessum");
String activo_sum = (String) request.getAttribute("sumActivo");
String pasivo_sum = (String) request.getAttribute("sumPasivo");
String dataColores = (String) request.getAttribute("dataColores");
String idUserInteligo = (String)themeDisplay.getUser().getExpandoBridge().getAttribute("ID_USER_INTELIGO");

Map<String, String> pasivo = (Map<String, String>) request.getAttribute("pasivo");
Map<String, List<Account>> mapperCuentas = (Map<String, List<Account>>) request.getAttribute("cash");
Map<String, String> mapperCuentasSum = (Map<String, String>) request.getAttribute("mapperCuentasSum");
Map<String, Number> mapperInversiones = (Map<String, Number>) request.getAttribute("inversiones");
Map<String, String> portafoliosum = (Map<String, String>) request.getAttribute("portafoliosum");

Map<String, Number> mapperPasivo = (Map<String, Number>) request.getAttribute("pasivo");
List<String> listaColores = (List<String>) request.getAttribute("listaColores");

Calendar cal = Calendar.getInstance();
String fecha =  UtilitiesFormat.formatDate(cal.getTime(), "dd/MM/yyyy HH:mm:ss");
Map<String, Map<String, List<Portafolio>>> mapperPortafolio  = (Map<String, Map<String, List<Portafolio>>>) request.getAttribute("portafolio");

DecimalFormatSymbols symbols = new DecimalFormatSymbols(new Locale("es"));
symbols.setDecimalSeparator('.');
symbols.setGroupingSeparator(',');
String pattern = "#,###,##0.00";
DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);

%>
<div class="content_portal" >
	<div class="content_portal">
	<!-- HEADER-->
	<!--DETALLE-->
	<div class="content_left_portal">
		<div class="fecha_actualizacion">Actualizado el <%=fecha %></div>
		<div class="content_collapse_general">
			<!--TITULO DE LOS ITEM-->
			<div class="resumen_item_actual">
				<div class="titulo_item">Mi Patrimonio</div>
				<div class="total_item">
					<div class="subtitulo_item">Total ($)</div>
					<div class="monto_item"><%=patrimonio_sum %></div>

				</div>
				<div class="total_item">
					<div class="subtitulo_item">Ganancias ($)</div>
					<div class="monto_item"><span class="signo_mas_item">+</span>0,000,000</div>
				</div>
			</div>
			<!--FIN-->
			<!--COLLAPSE 1-->
			<div class="collapse1">	
			<div class="content_elemento" data-toggle="collapse" href="#collapseExample" aria-expanded="false">
					<div class="columna_1_item">Activos</div>
					<div class="columna_2_item"><%=activo_sum %></div>
					<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
					<div class="clearfix"></div>
			</div>
			<div class="collapse" id="collapseExample">
				<!--HERMANO 1-->
				<div class="detalle_elemento_collapse" data-toggle="collapse" id="_subInversiones" href="#Subcollapse_inversiones" aria-expanded="false" aria-controls="Subcollapse_inversiones">
					    <div class="columna_1_item">Inversiones</div>
						<div class="columna_2_item"><%=inversiones_sum %></div>
						<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
						<div class="clearfix"></div>
				</div>
				
				<!-- TODO inversiones por tipo -->
				<div class="collapse" id="Subcollapse_inversiones">
				
					<div id="patrimonio-clase-activo">
				
					<%
						for (Entry<String, Number> entry : mapperInversiones.entrySet()) {
						 Map<String, List<Portafolio>> mapportafolio = mapperPortafolio.get(entry.getKey()+"|"+"CLASE_ACTIVO");
					%>
						<div class="Subcollapse _portafolio" id="portafolioCLASE_ACTIVO<%=entry.getKey() %>" data-portafolio="<%=entry.getKey() %>" data-toggle="collapse" href="#Subcollapse1CLASE_ACTIVO<%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
							<div class="columna_1_item"><%=entry.getKey() %></div>
							<div class="columna_2_item"><%=decimalFormat.format(entry.getValue())%></div>
							<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
							<div class="clearfix"></div>
						</div>
						<div class="collapse" id="Subcollapse1CLASE_ACTIVO<%=entry.getKey().replace(" ", "_")%>">
						<%
										for (Entry<String, List<Portafolio>> entryportafolio : mapportafolio.entrySet()) {
											String[] separadorkeyPortafolio = entryportafolio.getKey().split(Pattern.quote("|")); 
											int codigoColor = Integer.parseInt(separadorkeyPortafolio[1]);
											String colorCssCalculado = listaColores.get(codigoColor);
									%>
								<div class="SubHijocollapse" data-toggle="collapse" href="#SubHijocollapseCLASE_ACTIVO<%=separadorkeyPortafolio[0].replace(" ", "_")%><%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
									<div class="columna_1_item"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(separadorkeyPortafolio[0])) %></div>
									<div class="columna_2_item">
									<span><%=portafoliosum.get(entry.getKey().replace(" ", "_")+"_"+entryportafolio.getKey()) %></span>
									<div class="cuadro_color_grafico" style="background-color: <%=colorCssCalculado %>;"></div>
									<div class="arrow_box">+ 0.07 </div>
									</div>
									<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
									<div class="clearfix"></div>
								</div>
								<div class="collapse" id="SubHijocollapseCLASE_ACTIVO<%=separadorkeyPortafolio[0].replace(" ", "_")%><%=entry.getKey().replace(" ", "_")%>">
									<div class="contentSubHijo">
								<% List<Portafolio> lstportafolio = entryportafolio.getValue();
									for(Portafolio ptr : lstportafolio){
								%>								
										<div class="SubHijoCollapse">
											<div class="columna_1_item">
												<div class="titulo_small_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProducttype())) %></div>
												<div class="nombre_empresa_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProductname())) %></div>
											</div>
											<div class="columna_2_item">
													<span><%=decimalFormat.format(ptr.getCurrentbalance()) %></span>
													<div class="arrow_box">+ 0.07 </div>
											</div>
											<div class="columna_3_item"><span class="signo_mas_item ">+</span>0,000,000</div>
											<div class="clearfix"></div>
										</div>
								<% } %>
									</div>
								</div>
								<% } %>
								
							</div>
						
					<%
						}
					%>
					
					</div>
					
					
					<div id="patrimonio-tipo-produto">
				
					<%
						for (Entry<String, Number> entry : mapperInversiones.entrySet()) {
						 Map<String, List<Portafolio>> mapportafolio = mapperPortafolio.get(entry.getKey()+"|"+"TIPO_DE_PRODUCTO");
						 System.out.println("asdasdasdas"+mapportafolio);
					%>
						<div class="Subcollapse _portafolio" id="portafolioTIPO_DE_PRODUCTO<%=entry.getKey() %>" data-toggle="collapse" data-portafolio="<%=entry.getKey() %>" href="#Subcollapse1TIPO_DE_PRODUCTO<%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
							<div class="columna_1_item"><%=entry.getKey() %></div>
							<div class="columna_2_item"><%=decimalFormat.format(entry.getValue())%></div>
							<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
							<div class="clearfix"></div>
						</div>
						<div class="collapse" id="Subcollapse1TIPO_DE_PRODUCTO<%=entry.getKey().replace(" ", "_")%>">
						<%
										for (Entry<String, List<Portafolio>> entryportafolio : mapportafolio.entrySet()) {
											String[] separadorkeyPortafolio = entryportafolio.getKey().split(Pattern.quote("|")); 
											int codigoColor = Integer.parseInt(separadorkeyPortafolio[1]);
											String colorCssCalculado = listaColores.get(codigoColor);
									%>
								<div class="SubHijocollapse" data-toggle="collapse" href="#SubHijocollapseTIPO_DE_PRODUCTO<%=separadorkeyPortafolio[0].replace(" ", "_")%><%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
									<div class="columna_1_item"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(separadorkeyPortafolio[0])) %></div>
									<div class="columna_2_item">
									<span><%=portafoliosum.get(entry.getKey().replace(" ", "_")+"_"+entryportafolio.getKey()) %></span>
									<div class="cuadro_color_grafico" style="background-color: <%=colorCssCalculado %>;"></div>
									<div class="arrow_box">+ 0.07 </div>
									</div>
									<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
									<div class="clearfix"></div>
								</div>
								<div class="collapse" id="SubHijocollapseTIPO_DE_PRODUCTO<%=separadorkeyPortafolio[0].replace(" ", "_")%><%=entry.getKey().replace(" ", "_")%>">
									<div class="contentSubHijo">
								<% List<Portafolio> lstportafolio = entryportafolio.getValue();
									for(Portafolio ptr : lstportafolio){
								%>								
										<div class="SubHijoCollapse">
											<div class="columna_1_item">
												<div class="titulo_small_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProducttype())) %></div>
												<div class="nombre_empresa_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProductname())) %></div>
											</div>
											<div class="columna_2_item">
													<span><%=decimalFormat.format(ptr.getCurrentbalance()) %></span>
													<div class="arrow_box">+ 0.07 </div>
											</div>
											<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
											<div class="clearfix"></div>
										</div>
								<% } %>
									</div>
								</div>
								<% } %>
								
							</div>
						
					<%
						}
					%>
					
					</div>
					
					
					
					<div id="patrimonio-todos">
				
					
					<%
						for (Entry<String, Number> entry : mapperInversiones.entrySet()) {
						 Map<String, List<Portafolio>> mapportafolio = mapperPortafolio.get(entry.getKey()+"|"+"TODOS_INVERSIONES");
					%>
						<div class="Subcollapse _portafolio" id="portafolioTODOS_INVERSIONES<%=entry.getKey() %>" data-toggle="collapse" data-portafolio="<%=entry.getKey() %>" href="#Subcollapse1TODO<%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
							<div class="columna_1_item"><%=entry.getKey() %></div>
							<div class="columna_2_item"><%=decimalFormat.format(entry.getValue())%></div>
							<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
							<div class="clearfix"></div>
						</div>
						<div class="collapse" id="Subcollapse1TODO<%=entry.getKey().replace(" ", "_")%>">
						<%
										for (Entry<String, List<Portafolio>> entryportafolio : mapportafolio.entrySet()) {
									%>
								
									<div class="contentSubHijo">
								<% List<Portafolio> lstportafolio = entryportafolio.getValue();
									for(Portafolio ptr : lstportafolio){
										String colorCssCalculado = listaColores.get(ptr.getIndiceColor());
								%>								
										<div class="Subcollapse">
											<div class="columna_1_item">
												<div class="titulo_small_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProducttype())) %></div>
												<div class="nombre_empresa_columna"><%=StringUtil.upperCaseFirstLetter(StringUtil.lowerCase(ptr.getProductname())) %></div>
											</div>
											<div class="columna_2_item">
													<span><%=decimalFormat.format(ptr.getCurrentbalance()) %></span>
													<div class="cuadro_color_grafico" style="background-color: <%=colorCssCalculado %>;"></div>
													<div class="arrow_box">+ 0.07 </div>
											</div>
											<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
											<div class="clearfix"></div>
										</div>
								<% } %>
									</div>
								<% } %>
								
							</div>
						
					<%
						}
					%>
					
					
					</div>
					
					
					
					
					
				</div>
				
				
				
				
				
					<!--HERMANO 2-->
					<div class="detalle_elemento_collapse" data-toggle="collapse" href="#Subcollapse_cash" aria-expanded="false" aria-controls="Subcollapse_cash">
					    <div class="columna_1_item">Cash</div>
						<div class="columna_2_item"><%=cash_sum %></div>
						<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
						<div class="clearfix"></div>
					</div>
					<div class="collapse" id="Subcollapse_cash">
					<% 
						for (Entry<String, List<Account>> entry : mapperCuentas.entrySet()) { 
					%>
						<div class="Subcollapse" data-toggle="collapse" href="#SubHijocollapse_<%=entry.getKey().replace(" ", "_")%>" aria-expanded="false">
							<div class="columna_1_item"><%=entry.getKey() %></div>
							<div class="columna_2_item"><%=mapperCuentasSum.get(entry.getKey()) %></div>
							<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
							<div class="clearfix"></div>
						</div>
						<div class="collapse" id="SubHijocollapse_<%=entry.getKey().replace(" ", "_")%>">
							<div class="contentSubHijo">
							<% 
							List<Account> lstaccount = entry.getValue();
								for (Account obja : lstaccount) {
							%>
								<div class="SubHijoCollapse">
									<div class="columna_1_item">
										<div class="nombre_empresa_columna"><a data-numerocuenta="<%=obja.getAccountno()%>"><%=obja.getAccountno()%></a></div>
									</div>
									<div class="columna_2_item">
											<span><%=decimalFormat.format(obja.getWorkingbal())%></span>
											<div class="abrir_pop_transferir activadorPopupTransferencia" data-numerocuenta="<%=obja.getAccountno()%>" data-typeTransference="<%=entry.getKey() %>"> 
												<span>Transferir</span>
												<div class="flecha_transferir">
													<img src="<%= request.getContextPath()%>/images/transferir.png"/>
												</div>							
											</div>
									</div>
									<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
									<div class="clearfix"></div>
									<div class="content_pop_up_transferencias">
									
									  <div class="panel_transferencia_dv">
										<div class="txt_pop_transf">Deseo Transferir $</div>
										<input type="text" name="" class="form-control monto_dinero_transferir txtmontotrans" />
										<div class="form-validator-stack help-block hide" style="color: #a94442;font-size: 16px;">
										    <div role="alert" class="required">Ingrese un monto</div>
										</div>
										<div class="row_neg">
											<div class="linea_separadora"></div>
										</div>
										<br/>
										<div class="txt_pop_transf">Desde mi <b><%=obja.getAccounttype() %></b> a:</div>
										<br/>
										<% if(obja.getAccounttype().equals("Cuenta Corriente")){
											%>
												<select class="form-control cbonumcuenta">
											<% 
												for (Entry<String, List<Account>> entryTransferencia : mapperCuentas.entrySet()) {
													List<Account> listacuentastrans = entryTransferencia.getValue();
														for (Account objtrans : listacuentastrans) {
															if(objtrans.getAcccustomer().equals(idUserInteligo) 
																	&& Validator.isNotNull(objtrans.getAccountassoc())){
																
																if(objtrans.getAccountassoc().equals(obja.getAccountno())){
															%>
																<option value='<%=objtrans.getAccountno()%>'>
																	<%=objtrans.getAccounttype()%>: <%=objtrans.getAccountno()%>
																</option>
															<%
																}else{
																%>
																<option value='<%=objtrans.getAccountassoc()%>'>
																	<%=objtrans.getCollateralactive()%>
																</option>
																<%
																}
															}
														}
												}
											%>
												</select>
										<% 
										}else if(obja.getAccounttype().equals("Cuenta Trading")){%>
											<div class="lugar_transferir" data-type="<%=obja.getAccountassoc() %>">
												Cuenta corriente
												<span><%=obja.getAccountassoc() %></span>
											</div>
										<%}%>
										 
										<div class="loading-icon loading-icon-md hide"></div>
										<button class="btn_aceptar_transferir btn_transferencia_dev">Transferir</button>					
									   </div>
									  
									  <div class="panel_thankyoupage_dv hide">
									        <div class="alert alert-dismissible alert-notification alert-info" role="alert">
									            <div class="user-icon user-icon-info user-icon-xl center-block">
									                <span><span aria-hidden="true" class="glyphicon glyphicon-ok"></span></span>
									            </div>
									            <br>
									            <span style="font-size: 21px;display: block;text-align: center;">Transferencia completada</span>
									        </div>
									        <button class="btn btn-primary center-block btncerrarpopuptrans" type="button" data-toggle="modal" data-target="#myModal">Finalizar</button>
											<div class="loading-icon loading-icon-md panel_loading_finalizar hide"></div>
									   </div>
									  
									</div>
								</div>
							<%
								}
							%>
							</div>
						</div>
					<% 
						}
					%>
					</div>
					
			</div>	
			</div>		
			<!--COLLAPSE 2-->
			<div class="content_elemento"  data-toggle="collapse" href="#collapseExample_pasivo" aria-expanded="false">
				<div class="columna_1_item">Pasivos</div>
				<div class="columna_2_item"><%=pasivo_sum %></div>
				<div class="clearfix"></div>
			</div>
			
			<div class="collapse" id="collapseExample_pasivo">
			<%
			for (Entry<String, Number> entry : mapperPasivo.entrySet()) {
			%>
					<div class="detalle_elemento_collapse pasivo_p" >
					    <div class="columna_1_item"><%=entry.getKey() %></div>
						<div class="columna_2_item"><%=decimalFormat.format(entry.getValue()) %></div>
						<div class="columna_3_item"><span class="signo_mas_item">+</span>0,000,000</div>
						<div class="clearfix"></div>
					</div>
			<% } %>
			</div>
			<!--FIN-->
		</div>
		<div class="content_detalle_tarjeta">
			<div class="titulo_tarjeta">Tarjeta de cr�dito Visa 123456 </div>
			<div class="clearfix"></div>
			<div class="content_total_barra">
				<div class="barra_estado"></div>
			</div>
			<div class="saldo_disponible">Disponible: $8.000</div>
			<div class="saldo_usado">Usado: $2.000</div>
		</div>
	</div>
	<!-- LADO DERECHO-->
	<div class="content_right_portal">
		<div class="TabInicialRango" id="TabInicialRango">
			<div class="content_tabs_rango">
				<a href="#" class="rango_tab rango_tab_active">HOY</a>
				<div class="separador"></div>
				<a href="#" class="rango_tab">�LTIMA SEMANA</a>
				<div class="separador"></div>
				<a href="#" class="rango_tab">�LTIMO MES</a>
			</div>
			<div class="content_datos_rango">
				<div class="titulo_rango">Mejores desempe�os</div>
				<div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="titulo_rango">Desempe�os m�s bajos</div>
				<div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
					<div class="bloque_detalle_rango">
						Alicorp SA-Comm�n
						<div class="arrow_box">+ 0.07 </div>
					</div>
				</div>
				
			</div>		
		</div>
		<div class="TabGraficos" id="TabGraficos">
			<div class="content_tab_grafico">
				<!--
				<div class="content_btn_rango"><a href="#" class="rango_tab">RANGO DE FECHAS</a></div>				
				<div class="content_btn_rango"><a href="#" class="rango_tab rango_tab_active">MES ANTERIOR</a></div>-->				
				<div class="content_btn_rango" style="width: 100%;"><a href="#" class="rango_tab">ACTUAL</a></div>
			</div>
			<div class="content_sub_tabs">
				<a href="#" class="sub_tab_detalle subtab_active" id="tabpatrimonioclaseactivo">Por clase de activo</a>
				<a href="#" class="sub_tab_detalle" id="tabpatrimoniotipoproducto">Por tipo de producto</a>
				<a href="#" class="sub_tab_detalle" id="tabpatrimoniotodos">Todas mis inversiones</a>
			</div>
			<div class="content_grafico_detalle">
				<div class="content_tab_1">
					<div class="titulo_tab" id="titulo_tab">Composici�n portafolio Renta Fija</div>
					<div class="detalle_grafico">
						<div style="width:90%; margin:0 auto">
				        	<!--Div that will hold the dashboard-->
							<div id="chartContainer" style="height: 420px; max-width: 920px; margin: 0px auto;"></div>
							<!--  <canvas id="chart-area" width="300" height="300"/>-->
				    	</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	<div class="content_fondo_pop_up" id="PopUpTransferir">
		
	</div>
	
	
	<!--POP UP-->
	<div class='content_fondo_pop_up' id='PopMovimientos'>
		<div class='content_detalle_movimientos'>
			<div class='content_header_pop'>
				<div class='titulo_pop'>
					Cuenta Corriente
					<span><div id='div_data_numero_cuenta'></div></span>
				</div>
				<div class='cerrar_pop' id='CerrarPopUp'><i class='fa fa-times' aria-hidden='true'></i></div>
			</div>
			<div class='content_mes'>
				<div class='vista_mes_actual'>HOY</div>
			</div>
			<!-- TITULO DE LOS ELEMENTOS-->
			
			<div class="loading-icon loading-icon-md panel_loading_movimiento"></div>
			
			<div class='content_tabla_movimientos hide'>
			
			<div id="movimiento_tabla">
			</div>
			
			
			</div>
		</div>
	</div>


</div>
	
</div>

<div class="modal fade" id="myModal" role="dialog" style="display:none;">
	<div class="modal-dialog" style="position:fixed; top:40%; left:40%; width:300px; height=150px;">
		<div class="modal-content">
			<div class="modal-body">
				<table>
					<tr><td><img src="<%= request.getContextPath()%>/images/ajax-loader.gif" alt="Actualizando saldos" width="263px"></td></tr>
					<tr><td align="center">Actualizando Saldos</td></tr>
				</table>
			</div>
		 </div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	var varglobalnumeroportafolio;
	var varglobaltabportafolio;
	$("#patrimonio-clase-activo").hide();
	$("#patrimonio-tipo-produto").hide();
	$("#patrimonio-todos").hide();
	$('#TabInicialRango').show();
	
	actualizarListaPatrimonio('tabpatrimonioclaseactivo');
	
	
	
	
	function actualizarListaPatrimonio(PatrimonioTab){

		if(PatrimonioTab=='tabpatrimonioclaseactivo'){
			//TODO
			$("#patrimonio-clase-activo").show();
			$("#patrimonio-tipo-produto").hide();
			$("#patrimonio-todos").hide();
		}
		if(PatrimonioTab=='tabpatrimoniotipoproducto'){
			$("#patrimonio-clase-activo").hide();
			$("#patrimonio-tipo-produto").show();
			$("#patrimonio-todos").hide();
		}
		if(PatrimonioTab=='tabpatrimoniotodos'){
			$("#patrimonio-clase-activo").hide();
			$("#patrimonio-tipo-produto").hide();
			$("#patrimonio-todos").show();
		}
		
		varglobaltabportafolio = PatrimonioTab;
		
		
		
		
		
	}
	
	
	function dibujarGrafico(PatrimonioTab){
		var valorpatrimoniotab;
		var dataJsonGraficoServer = <%=dataxActivo %>;
		if(PatrimonioTab=='tabpatrimonioclaseactivo'){
			dataJsonGraficoServer = <%=dataxActivo %>;
		}
		if(PatrimonioTab=='tabpatrimoniotipoproducto'){
			dataJsonGraficoServer = <%=dataxProducto %>;
		}
		if(PatrimonioTab=='tabpatrimoniotodos'){
			dataJsonGraficoServer = <%=dataxTodos %>;
		}
		
		var dataColores =<%=dataColores %>;
		console.log(dataJsonGraficoServer);
		
		
		var dataJsonGraficoServerFiltered = dataJsonGraficoServer.filter(function (entry) {
		    return entry.portafolio === varglobalnumeroportafolio;
		});
		
		console.log(varglobalnumeroportafolio);
		console.log(dataJsonGraficoServerFiltered);
		
		var dataChart = [];
		var dataColorsChart = [];
		
		
		$.each(dataJsonGraficoServerFiltered, function(index, value) {
			dataChart.push({label: value.producto, y: value.Saldo});
			dataColorsChart.push(dataColores[value.color].color);
			
	    });
		
		
		CanvasJS.addColorSet("colorShades",
				dataColorsChart);
		
		var chart = new CanvasJS.Chart("chartContainer", {
			colorSet: "colorShades",
			animationEnabled: true,
			data: [{
				type: "pie",
				startAngle: 60,
				indexLabelFontSize: 14,
				indexLabel: "#percent%",
				dataPoints: dataChart,
			}]
		});
		chart.render();
		
		/*
		var pieData = [
			{
				value: 300,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Red"
			},
			{
				value: 50,
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Green"
			},
			{
				value: 100,
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Yellow"
			},
			{
				value: 40,
				color: "#949FB1",
				highlight: "#A8B3C5",
				label: "Grey"
			},
			{
				value: 120,
				color: "#4D5360",
				highlight: "#616774",
				label: "Dark Grey"
			}

		];
		
		var ctx = document.getElementById("chart-area").getContext("2d");
		window.myPie = new Chart(ctx).Pie(pieData);
		*/
		
		
		
		
	}
	//inicio popup movimientos
	
	$(".nombre_empresa_columna a").click(function(e) {
		e.preventDefault();
		var varnumerocuenta = $(this).attr('data-numerocuenta');
		$("#div_data_numero_cuenta").html('');
		$("#div_data_movimiento").html('');
		
		$("#div_data_numero_cuenta").append(varnumerocuenta);
		
		$(".content_tabla_movimientos").addClass('hide');
		$('.panel_loading_movimiento').removeClass('hide');
		
		$("#PopMovimientos").show();  
		
		
		
		$.ajax({
            type : 'POST',
            url  : '<%=sendData%>',
            data: { 
            	<portlet:namespace/>cmd : "getMovimientos",
            	<portlet:namespace/>numeroCuenta : varnumerocuenta,
            	<portlet:namespace/>customerNo :'<%= idUserInteligo %>'
            },
            success : function(data){
            	var data1 = JSON.parse(data);
            	console.log(data1);
            	
            	
            $("#movimiento_tabla").html('');
            	
			var strHtmlMovimiento ="<div class='content_datos_tabla'>"+
			"<div class='fecha_movimiento'>Fecha</div>"+
			"<div class='descripcion_movimiento'>Descripci�n</div>"+
			"<div class='cargo_movimiento'>Cargos $</div>"+
			"<div class='abono_movimiento'>Abonos $</div>"+
			"<div class='saldo_movimiento'>Saldo $</div>"+
			"</div>";
            	
            	
            	
            	
                $.each(data1, function(index, value) {
//                 	$("#div_data_movimiento").append(
                		strHtmlMovimiento += "<div class='content_datos_tabla'>";
                		strHtmlMovimiento += "<div class='fecha_movimiento'>"+value.ProcessDate+"</div>";
                		strHtmlMovimiento += "<div class='descripcion_movimiento'>"+value.Description+"</div>";
                		strHtmlMovimiento += "<div class='cargo_movimiento'>"+value.Debit+"</div>";
                		strHtmlMovimiento += "<div class='abono_movimiento'>"+value.Credit+"</div>";
                		strHtmlMovimiento += "<div class='saldo_movimiento'>"+value.Balance+"</div>";
                		strHtmlMovimiento += "</div>";
                	
                	
        	    });
        	    $("#movimiento_tabla").append(strHtmlMovimiento);
        	    
                $('.panel_loading_movimiento').addClass('hide');
            	$(".content_tabla_movimientos").removeClass('hide');
            }
       });
		
    });
	
	$("#CerrarPopUp").on("click", function () {
        $("#PopMovimientos").hide();           
    });
	
	//fin popup movimientos
	

	//tabs
	 $("#tabpatrimonioclaseactivo").click(function(e) {
		 e.preventDefault();
		 if ( $('.portafolioselected').length > 0) {
			     // do something 
			 //alert(varglobalnumeroportafolio);
			 dibujarGrafico('tabpatrimonioclaseactivo');
			 actualizarListaPatrimonio('tabpatrimonioclaseactivo');
		 }
	 });
	
	 $("#tabpatrimoniotipoproducto").click(function(e) {
		 e.preventDefault();
		 if ( $('.portafolioselected').length > 0) {
			     // do something 
			 //alert(varglobalnumeroportafolio);
			 dibujarGrafico('tabpatrimoniotipoproducto');
			 actualizarListaPatrimonio('tabpatrimoniotipoproducto');
		 }
	 });
	 
	 $("#tabpatrimoniotodos").click(function(e) {
		 e.preventDefault();	
		 if ( $('.portafolioselected').length > 0) {
		     // do something 
			 //alert(varglobalnumeroportafolio);
			 dibujarGrafico('tabpatrimoniotodos');
			 actualizarListaPatrimonio('tabpatrimoniotodos');
		 }
	 });
	
	
	//var input = $("<c:out value='${MapclassActiveJSON}' />");
    //console.log(input);
	
	$('#TabGraficos').hide();
	
	var data = $.parseJSON( '{"resultadoDesempenio":{"tablaMejorDesempenio":[{"ProductName":"Company","RealDist":"8.78%"},{"ProductName":"Company","RealDist":"8.78%"},{"ProductName":"Company","RealDist":"8.78%"},{"ProductName":"Company","RealDist":"8.78%"}],"tablaPeorDesempenio":[{"ProductName":"Company","RealDist":"8.79%"},{"ProductName":"Company","RealDist":"8.79%"},{"ProductName":"Company","RealDist":"8.79%"},{"ProductName":"Company","RealDist":"8.79%"}]}}' );
	//inicio carga desempe�o
	$.each(data.resultadoDesempenio.tablaMejorDesempenio, function(index, value) {
    		   $("#idMejorDesempenio").append("<div class='bloque_detalle_rango'>"+value.ProductName+
    				   "<div class='arrow_box'>"+value.RealDist+"</div></div>");
    });
    //obtener desempe�o negativo
    $.each(data.resultadoDesempenio.tablaPeorDesempenio, function(index, value) {
    		   $("#idPeorDesempenio").append("<div class='bloque_detalle_rango'>"+value.ProductName+
    				   "<div class='arrow_box'>"+value.RealDist+"</div></div>");
    });
	//fin carga desempe�o
	
	$("._portafolio").click(function (e) {
		e.preventDefault();
		var varnumeroportafolio = $(this).attr('data-portafolio');
		varglobalnumeroportafolio = varnumeroportafolio;
		
		$(this).toggleClass('portafolioselected');	
		
		
		if ( $( this ).hasClass( "portafolioselected" ) ) {
			

			 if (!$("#portafolioCLASE_ACTIVO"+varglobalnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioCLASE_ACTIVO"+varglobalnumeroportafolio).click();
			 }
			if (!$("#portafolioTIPO_DE_PRODUCTO"+varglobalnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioTIPO_DE_PRODUCTO"+varglobalnumeroportafolio).click();
			 }
			if (!$("#portafolioTODOS_INVERSIONES"+varglobalnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioTODOS_INVERSIONES"+varglobalnumeroportafolio).click();
			 }
			
			
			
    		
    		$("#titulo_tab").html('');
    		$("#titulo_tab").append('Composici�n portafolio '+varglobalnumeroportafolio);
    		//TODO
    		if ($("#tabpatrimonioclaseactivo").hasClass( "subtab_active" ) ) {
    			dibujarGrafico('tabpatrimonioclaseactivo');
    		}
    		if ($("#tabpatrimoniotipoproducto").hasClass( "subtab_active" ) ) {
    			dibujarGrafico('tabpatrimoniotipoproducto');
    		}
    		if ($("#tabpatrimoniotodos").hasClass( "subtab_active" ) ) {
    			dibujarGrafico('tabpatrimoniotodos');
    		}
    		
    		$('#TabGraficos').show();
    		$('#TabInicialRango').hide();
    		
    		
		}
		else{
			//ocultar tabs seleccionados del patrimonio seleccionado
			if ($("#portafolioCLASE_ACTIVO"+varnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioCLASE_ACTIVO"+varnumeroportafolio).click();
			 }
			if ($("#portafolioTIPO_DE_PRODUCTO"+varnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioTIPO_DE_PRODUCTO"+varnumeroportafolio).click();
			 }
			if ($("#portafolioTODOS_INVERSIONES"+varnumeroportafolio).hasClass("portafolioselected") ) {
				 $("#portafolioTODOS_INVERSIONES"+varnumeroportafolio).click();
			 }
		}
		
		
		if ($(".portafolioselected").length == 0) {
			$('#TabGraficos').hide();
    		$('#TabInicialRango').show();
		}
		
		
		
	  });
	
	
	
	

	
/////////////////////////////////////////////////////////////////////////////
// 	DEV TRANSFERENCIA
////////////////////////////////////////////////////////////////////////////

	var customerNoGlobal = '';
	var accountNoGlobal = '';
	
	$("body").on("click", '.activadorPopupTransferencia', function () {
    	
		console.info('click first popup');
		
		var este = this;
		
		//set params
		var customerNo = '<%= idUserInteligo %>';
		customerNoGlobal = customerNo;
		var accountNo = $(este).attr('data-numerocuenta');
		accountNoGlobal = accountNo;

//		SHOW POPUP TRANSFERENCIA
		$("#PopUpTransferir").show();
		$(este).parent().parent().children('.content_pop_up_transferencias').show();
		$(este).css('z-index','1001');
				
	});

    $("body").on("click",'.btn_transferencia_dev', function () {
		
    	var este = this;
    	
//    	VALIDAR MONTO
		var monto = $(este).siblings('.txtmontotrans').val();
		var cboValue = $(este).siblings('.lugar_transferir').attr('data-type');
		
		if(cboValue != null && cboValue != ""){
			cboValue = cboValue;
		}else{
			cboValue = $(este).siblings('.cbonumcuenta').val();
		}
		
		console.info(cboValue);
		
    	if(monto == '' || monto == null || !$.isNumeric(monto)){
    		$(este).siblings('.form-validator-stack').removeClass('hide');
    	}else{
    		
//    		Ocultar mensaje error del monto 
    		$(este).siblings('.form-validator-stack').addClass('hide');
//    		Ocultar Loader    	
			$(este).siblings('.loading-icon').removeClass('hide');
			
			//set params
			var customerNo = customerNoGlobal;
			var accountFrom = accountNoGlobal;
			var payer = '<%=(String)themeDisplay.getUser().getFullName()%>';
			var accountTo = cboValue;
			var debitAmt = monto;
			var debitCcy = 'USD';
			var creditCcy = 'USD';
			
			$.ajax({
				method: "POST",
				url : '<%=sendData%>',
				data : {<%=renderResponse.getNamespace()%>cmd: 'transferencia', 
						<%=renderResponse.getNamespace()%>customerNo: customerNo,
						<%=renderResponse.getNamespace()%>accountFrom: accountFrom,
						<%=renderResponse.getNamespace()%>debitCcy: debitCcy,
						<%=renderResponse.getNamespace()%>debitAmt: debitAmt,
						<%=renderResponse.getNamespace()%>payer: payer,
						<%=renderResponse.getNamespace()%>accountTo: accountTo,
						<%=renderResponse.getNamespace()%>creditCcy:creditCcy
				},
				dataType: "json",
				success : function(data) {
					if(data._returncode == 0){
//						OCULTAR LOADING
						$(este).siblings('.loading-icon').addClass('hide');
//						SHOW THANKYOU_PAGE
						$(este).parent().addClass('hide');
						$(este).parent().siblings('.panel_thankyoupage_dv').removeClass('hide');
					}
				}
			});
    	}
	 });
    
    $("body").on("click",'.btncerrarpopuptrans', function () {
    	//$(this).siblings('.panel_loading_finalizar').removeClass('hide');
    	$('.content_pop_up_transferencias').addClass('hide');
    	location.reload();
// 		$("#PopUpTransferir").hide();
// 		$('.content_pop_up_transferencias').hide();
// 		$(".activadorPopupTransferencia").css('z-index','1');
		
// 		//reset values
// 		$('.monto_dinero_transferir').val('');
		
// 		//switch panels
// 		$('.panel_thankyoupage_dv').addClass('hide');;
// 		$('.panel_transferencia_dv').removeClass('hide');
		
	 });
    
    $("#PopUpTransferir").on("click", function () {
		$("#PopUpTransferir").hide();
		$('.content_pop_up_transferencias').hide();
		$(".activadorPopupTransferencia").css('z-index','1');
		
		//reset values
		$('.monto_dinero_transferir').val('');
		$('.loading-icon').addClass('hide');
		$('.form-validator-stack').addClass('hide');
		
		//switch panels
		$('.panel_thankyoupage_dv').addClass('hide');;
		$('.panel_transferencia_dv').removeClass('hide');
	}); 
    
    
    /*$("#PopMovimientos").on("click", function () {
		$("#PopMovimientos").hide();
		$('.panel_loading_movimiento').removeClass('hide');
    	$(".content_tabla_movimientos").addClass('hide');
		//$('.loading-icon').addClass('show');
	});*/ 
	
	$(".txtmontotrans").on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value ) {
                return value.replace(/\D/g, "")
                            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, "");
            });
        }
    });
    
});
</script>