package inteligo.job.poc.portlet;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.messaging.BaseSchedulerEntryMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelper;
import com.liferay.portal.kernel.scheduler.TimeUnit;
import com.liferay.portal.kernel.scheduler.TriggerFactory;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import inteligo.dto.AlertaVencimientoDTO;
import inteligo.dto.PrestamoDTO;
import inteligo.dto.PrestamoParentDTO;

@Component(immediate = true, service = InteligoJobPocPortlet.class)
public class InteligoJobPocPortlet extends BaseSchedulerEntryMessageListener {
	
	private static final String ALERT_ANTES_VENC = "ALERT_ANTES_VENC";
	private static final String ALERT_PROX_CUOT = "ALERT_PROX_CUOT";
	private static final int CONSTANT_ALERT_PROX_CUOT = 0;

	@Override
	protected void doReceive(Message message) throws Exception {

		final String REST_BASE_URI = "http://localhost:8081/RESTlocal/listprestamo";
		System.out.println("test");
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName(StringPool.UTF8)));
		
		ResponseEntity<String> responsePrestamos = restTemplate.exchange(REST_BASE_URI,HttpMethod.GET, new HttpEntity<String>(headers), String.class);
		String mockupPrestamos = responsePrestamos.getBody();
		
		Gson gson = new Gson();
		PrestamoParentDTO prestamosDTO = gson.fromJson(mockupPrestamos, PrestamoParentDTO.class);
		
		for (PrestamoDTO presServicio : prestamosDTO.getAlertas()) {
			
			UserLocalService userLocalService = UserLocalServiceUtil.getService();
			
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			
			try {
				
				User userPortal = userLocalService.getUserByScreenName(company.getCompanyId(), "c".concat(presServicio.getIdclie()));
				
				PermissionChecker  permissionChecker = PermissionCheckerFactoryUtil.create(userPortal);
				PermissionThreadLocal.setPermissionChecker(permissionChecker);
				
//				Tratamiento para la Alerta de antes de vencimiento
				if(Validator.isNotNull(userPortal.getExpandoBridge().getAttribute(ALERT_ANTES_VENC))){

					Type listType = new TypeToken<ArrayList<AlertaVencimientoDTO>>(){}.getType();
					
					List<AlertaVencimientoDTO> alertAntesVencDTO = gson.fromJson(
							(String)userPortal.getExpandoBridge().getAttribute(ALERT_ANTES_VENC), listType);
					
					for (AlertaVencimientoDTO alertAntesVenc : alertAntesVencDTO) {

						if(presServicio.getCodpro().equals(alertAntesVenc.getCodpre()) && !alertAntesVenc.isEnviado()){
							
//									Si el día restante a vencer la deuda es menor al de notificar -> triger
							if(presServicio.getDiares() == alertAntesVenc.getAlertantes()){
								System.out.println("	** send email alerta antes delvencimiento**");
//										sendEmail("alertas2@inteligogroup.com", "mshelzr@gmail.com".split(";"), 
//												"Próximo vencimiento de la cuota de mi tarjeta de crédito"
//												, "lorem ipsum dolor sit amet consectetur adipiscing elit ");
								alertAntesVenc.setEnviado(Boolean.TRUE);
							}
						}
					}
					
					userPortal.getExpandoBridge().setAttribute(ALERT_ANTES_VENC, gson.toJson(alertAntesVencDTO));
				}
				
//				Tratamiento para la Alerta de la próxima cuota
				if(Validator.isNotNull(userPortal.getExpandoBridge().getAttribute(ALERT_PROX_CUOT))){

					Type listType = new TypeToken<ArrayList<AlertaVencimientoDTO>>(){}.getType();
					
					List<AlertaVencimientoDTO> alertAntesVencDTO = gson.fromJson(
							(String)userPortal.getExpandoBridge().getAttribute(ALERT_PROX_CUOT), listType);
					
					for (AlertaVencimientoDTO alert : alertAntesVencDTO) {

						if(presServicio.getCodpro().equals(alert.getCodpre()) && !alert.isEnviado()){
							
							if(presServicio.getDiares() < CONSTANT_ALERT_PROX_CUOT){
								System.out.println("	** send email alerta prox cuota**");
//										sendEmail("alertas2@inteligogroup.com", "mshelzr@gmail.com".split(";"), 
//												"Próximo vencimiento de la cuota de mi tarjeta de crédito"
//												, "lorem ipsum dolor sit amet consectetur adipiscing elit ");
								alert.setEnviado(Boolean.TRUE);
							}
							
						}
					}
					
					userPortal.getExpandoBridge().setAttribute(ALERT_PROX_CUOT, gson.toJson(alertAntesVencDTO));
				}
			} catch (PortalException e) {
				
			}
		}
		
	}

	@Activate
	@Modified
	protected void activate() {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		//SE ESTABLECE LA HORA DEL NACIMIENTO DEL JOB
		calendar.set(Calendar.HOUR_OF_DAY, 7);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		
//		A PARTIR DE ESA HORA, SE DISPARA CADA 12 HORAS, ES DECIR A LAS 7AM DEL PRÓXIMO DÍA
		schedulerEntryImpl.setTrigger(TriggerFactoryUtil.createTrigger(getEventListenerClass(), 
				getEventListenerClass(), calendar.getTime(), 12, TimeUnit.HOUR));

		_schedulerEngineHelper.register(this, schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);
	}

	@Deactivate
	protected void deactivate() {
		_schedulerEngineHelper.unregister(this);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(
		ModuleServiceLifecycle moduleServiceLifecycle) {
	}

	
	@Reference(unbind = "-")
	protected void setSchedulerEngineHelper(SchedulerEngineHelper schedulerEngineHelper) {

		_schedulerEngineHelper = schedulerEngineHelper;
	}

	@Reference(unbind = "-")
	protected void setTriggerFactory(TriggerFactory triggerFactory) {
	}

	private SchedulerEngineHelper _schedulerEngineHelper;
	

	
	public static void sendEmail(String fromAddress, String[] toAddress, String subject, String body) throws Exception {

		try {
			
			MailMessage mailMessage = new MailMessage();
			
			InternetAddress[] toAdressArray = new InternetAddress[toAddress.length];
			
			int contador = 0;
			
			for (String strToAddress : toAddress) {
				toAdressArray[contador] = new InternetAddress(strToAddress);
				contador++;
			}
			
			mailMessage.setTo(toAdressArray);
			mailMessage.setFrom(new InternetAddress(fromAddress));
			mailMessage.setSubject(subject);
			mailMessage.setBody(body);
			MailServiceUtil.sendEmail(mailMessage);
			
		} catch (AddressException e) {
			e.printStackTrace();
		} 
		
	}

}