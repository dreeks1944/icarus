package inteligo.dto;

public class AlertaVencimientoDTO {
	
	private String codpre;
	private int alertantes;
	private boolean enviado;

	public AlertaVencimientoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getCodpre() {
		return codpre;
	}
	public void setCodpre(String codpre) {
		this.codpre = codpre;
	}
	public int getAlertantes() {
		return alertantes;
	}
	public void setAlertantes(int alertantes) {
		this.alertantes = alertantes;
	}

	public boolean isEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public AlertaVencimientoDTO(String codpre, int alertantes, boolean enviado) {
		super();
		this.codpre = codpre;
		this.alertantes = alertantes;
		this.enviado = enviado;
	}

	@Override
	public String toString() {
		return "AlertAntesVencDTO [codpre=" + codpre + ", alertantes=" + alertantes + ", enviado=" + enviado + "]";
	}
	
	
}
