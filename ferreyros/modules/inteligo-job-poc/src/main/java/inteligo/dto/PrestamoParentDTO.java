package inteligo.dto;

import java.util.List;

public class PrestamoParentDTO {

	private List<PrestamoDTO> alertas;

	public PrestamoParentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<PrestamoDTO> getAlertas() {
		return alertas;
	}

	public void setAlertas(List<PrestamoDTO> alertas) {
		this.alertas = alertas;
	}

	@Override
	public String toString() {
		return "PrestamoParentDTO [alertas=" + alertas + "]";
	}

	public PrestamoParentDTO(List<PrestamoDTO> alertas) {
		super();
		this.alertas = alertas;
	}
	
}
