package inteligo.dto;

public class PrestamoDTO {

	private String idclie;
	private String codpro;
	private int diares;
	
	public PrestamoDTO(String idclie, String codpro, int diares) {
		super();
		this.idclie = idclie;
		this.codpro = codpro;
		this.diares = diares;
	}
	
	public PrestamoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getIdclie() {
		return idclie;
	}

	public void setIdclie(String idclie) {
		this.idclie = idclie;
	}

	public String getCodpro() {
		return codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	public int getDiares() {
		return diares;
	}

	public void setDiares(int diares) {
		this.diares = diares;
	}

	@Override
	public String toString() {
		return "PrestamoDTO [idclie=" + idclie + ", codpre=" + codpro + ", diares=" + diares + "]";
	}
	
	
}