package programador.dto;

public class PrestamoDTO {

	private String idclie;
	private String codpre;
	private int diares;
	
	public PrestamoDTO(String idclie, String codpre, int diares) {
		super();
		this.idclie = idclie;
		this.codpre = codpre;
		this.diares = diares;
	}
	
	public PrestamoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getIdclie() {
		return idclie;
	}

	public void setIdclie(String idclie) {
		this.idclie = idclie;
	}

	public String getCodpre() {
		return codpre;
	}

	public void setCodpre(String codpre) {
		this.codpre = codpre;
	}

	public int getDiares() {
		return diares;
	}

	public void setDiares(int diares) {
		this.diares = diares;
	}

	@Override
	public String toString() {
		return "PrestamoDTO [idclie=" + idclie + ", codpre=" + codpre + ", diares=" + diares + "]";
	}
	
	
}