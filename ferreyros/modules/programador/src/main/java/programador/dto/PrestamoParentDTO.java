package programador.dto;

import java.util.List;

public class PrestamoParentDTO {

	private List<PrestamoDTO> prestamos;

	public PrestamoParentDTO(List<PrestamoDTO> prestamos) {
		super();
		this.prestamos = prestamos;
	}

	public PrestamoParentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<PrestamoDTO> getPrestamos() {
		return prestamos;
	}

	public void setPrestamos(List<PrestamoDTO> prestamos) {
		this.prestamos = prestamos;
	}

	@Override
	public String toString() {
		return "PrestamoParentDTO [prestamos=" + prestamos + "]";
	}
	
}
