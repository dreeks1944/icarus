package programador.dto;

public class AlertAntesVencDTO {
	
	private String codpre;
	private int alertantes;

	public AlertAntesVencDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AlertAntesVencDTO(String codpre, int alertantes) {
		super();
		this.codpre = codpre;
		this.alertantes = alertantes;
	}
	
	public String getCodpre() {
		return codpre;
	}
	public void setCodpre(String codpre) {
		this.codpre = codpre;
	}
	public int getAlertantes() {
		return alertantes;
	}
	public void setAlertantes(int alertantes) {
		this.alertantes = alertantes;
	}
	
	@Override
	public String toString() {
		return "AlertAntesVencDTO [codpre=" + codpre + ", alertantes=" + alertantes + "]";
	}
	
}
