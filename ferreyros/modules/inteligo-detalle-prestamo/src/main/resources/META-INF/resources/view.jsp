<%@ include file="/init.jsp"%>

<portlet:resourceURL var="sendDataPrestamo" />

<portlet:resourceURL var="sendExcelPrestamo">
	<portlet:param name="cmd" value="CommandExportPrestamoExcel"/>
</portlet:resourceURL>
<portlet:resourceURL var="sendPdfPrestamo">
	<portlet:param name="cmd" value="CommandExportPrestamoPdf"/>
</portlet:resourceURL>

  <!--Modal Pr�stamos-->
  <div class="modal fade modalPrestamos" id="modalPrestamos" role="dialog" style="display: none !important;">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <div class="container-fluid">
           <div class="row">
             <div class="col-md-11 col-sm-11 col-xs-11">
             	 <p id="detalle_tit_prestamo">Pr�stamo</p>
                 <p id="detalle_prestamo_descripcion">Pr�stamo Tipo 100023</p>
                 <p id="detalle_prestamo_moneda">Moneda: D�lares</p>
             </div>
             <div class="col-md-1 col-sm-1 col-xs-1 align-self-center text-center"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
           </div>
         </div>
       </div>
       <div class="modal-body">
         <div class="container-fluid">
           <div class="row">
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span >Saldo capital</span></div>
                <div class="amount-prestamo"><span  id="detalle_prestamo_saldo_capital"></span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span>Tasa inter�s</span></div>
                <div class="date-prestamo"><span id="detalle_prestamo_tasa_interes">0.8%</span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span>Fecha pr�ximo pago</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_fecha_pago">12 mar 2018</span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span>Monto del pr�stamo</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_monto_prestamo"></span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span>Fecha del desembolso</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_fecha_desembolso">12 feb 2017</span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span>Fecha vencimiento</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_fecha_vencimiento">12 jul 2017</span></div>
              </div>
              <div class="inf-prestamo frecap-prestamo">
                <div class="desc-prestamo"><span>Frec. Capital/Inter�s</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_frecuencia_capital_interes">Cada 12 meses</span></div>
              </div>
              <div class="inf-prestamo">
                <div class="desc-prestamo"><span> Intereses acumulados</span></div>
                <div class="amount-prestamo"><span id="detalle_prestamo_interes_acumulado">90.80%</span></div>
              </div>
            </div>
         </div>
         <div class="content-calendar-prestamos">
            <!-- Nav tabs -->
             <ul class="nav nav-tabs bg-white" role="tablist">
               <li class="nav-item">
                 <a id="linkCalendarioPrestamos" class="nav-link active" data-toggle="tab" href="#calendarPrestamos" role="tab">Calendario de pagos</a>
               </li>
             </ul>
             <!-- Tab panes -->
             <div class="tab-content content-calendar">
               <div class="tab-pane active" id="calendarPrestamos" role="tabpanel">

                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-7 bg-white border mt-4">
                        <table id="tablePrestamos" class="tablePrestamos fixed_headers">
                          <thead>
                            <tr>
                              <th>N�</th>
                              <th>Capital</th>
                              <th>Intereses</th>
                              <th>Monto</th>
                              <th>Fecha de pago</th>
                            </tr>
                          </thead>
                          <tbody id="body_calendario_pago_prestamo">
                            <tr class="paid-prestamo">
                              <td>1 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 ene 2018</td>
                            </tr>
                            <tr class="paid-prestamo">
                              <td>2 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 feb 2018</td>
                            </tr>
                            <tr class="pending-prestamo">
                              <td>3 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 mar 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>4 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 abr 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>5 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 may 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>6 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 jun 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>7 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 jul 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>8 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 ago 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>9 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 set 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>10 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 oct 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>11 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 set 2018</td>
                            </tr>
                            <tr class="nextPaid-prestamo">
                              <td>12 de 12</td>
                              <td>2,500.00</td>
                              <td>500.00</td>
                              <td>3,000.00</td>
                              <td>27 set 2018</td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="content-btn-prestamos d-flex justify-content-md-center">
                         <a href="<%=sendPdfPrestamo%>" target="_blank"><button id="pdf_prestamo_descargar"> <i class="fa fa-fw fa-download"></i> Imprimir</button></a>
						 <a href="<%=sendExcelPrestamo%>"><button id="excel_prestamo_descargar"> <i class="fa fa-fw fa-download"></i> Descargar Excel</button></a>
						 <button data-toggle="modal" data-target="#modalSendEmailPrestamo" id="sendEmailPrestamo"><i class="fa fa-fw fa-envelope-o"></i> Enviar Mail</button>
                        </div>
                      </div>
             <div class="col-md-5 mt-3">
               <div class="content-schedule-creditCard bg-white border">
                 <div class="schedule-header">
                   <p class="schedule-notificame">Notif�came</p>
                 </div>
                 <div class="schedule-body">
                   <div class="schedule-body-left">
                     <div class="schedule-procu">
                       <p>Cuando se haya cargado en mi cuenta la pr�xima cuota</p>
                     </div>
                     <div class="schedule-veprocu">
                       <div class="group-schedule-number">
                         <div class="input-number" min="1" max="8">
                           <input type="text" value="1" id="inputalertantesprestamo" />
                           <button class="input-number-increment" data-increment></button>
                           <button class="input-number-decrement" data-decrement></button>
                         </div>
                       </div>
                       <div class="schedule-desc">
                         <p>D�as antes del vencimiento de la pr�xima cuota</p>
                       </div>
                     </div>
                   </div>
                   <div class="schedule-body-right">
                     <span class="fa fa-fw fa-envelope-o schedule-mail"></span>
                     <div class="schedule-checkbox">
                       <input class="styled-checkbox" type="checkbox" id="iptChkPrestamo1" name="" value="">
                       <label  for="iptChkPrestamo1"></label>
                     </div>
                     <div class="schedule-checkbox">
                       <input class="styled-checkbox" type="checkbox" id="iptChkPrestamo2" name="" value="">
                       <label  for="iptChkPrestamo2"></label>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
                    </div>
                  </div>
               </div>
             </div>
          </div>
       </div>
    </div>
   </div>
  </div>
  <!-- Moda Pr�stamos-- -->
  
  <!-- Inicio - Modal Send Email-->
  
  <div class="modal fade modalSendEmail" id="modalSendEmailPrestamo" role="dialog" style="display: none;">
   <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header color-bk">
         <div class="container-fluid">
           <div class="row">
             <div class="col-md-11 col-sm-11 col-xs-11">
                 <p class="text-center"><strong>Enviar Mail</strong></p>
             </div>
             <div class="col-md-1 col-sm-1 col-xs-1 align-self-center text-center"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
           </div>
         </div>
       </div>
       <div class="modal-body">
         <form class="form-horizontal">
            <div class="form-group">
               <label class="control-label col-sm-2" for="destinatarioEmailPrestamo">Destinario</label>
               <div class="col-sm-10">
                 <input type="text" class="form-control" id="destinatarioEmailPrestamo">
               </div>
            </div>
            <div class="form-group">
               <label class="control-label col-sm-2" for="asuntoEmailPrestamo">Asunto</label>
               <div class="col-sm-10">
                 <input type="text" class="form-control" id="asuntoEmailPrestamo">
               </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="commentPrestamo"></label>
              <div class="col-sm-10">
                <textarea class="form-control" id="commentPrestamo" rows="3"></textarea>
              </div>
            </div>
            <div class="btn-g-sendEmail d-flex justify-content-md-center">
              <button id="enviar_email_prestamo"><i class="fa fa-fw fa-envelope-o"></i> Enviar Mail</button>
            </div>
         </form>
       </div>
    </div>
   </div>
  </div>
  



<!-- Fin - Modal Send Email-->


<script type="text/javascript">
	
var CONSTANTES_PRESTAMO = {ESTADO_PAGO_CANCELADO:"CANC",ESTADO_PAGO_POR_PAGAR:"PORPAG",ESTADO_PAGO_POR_PAGAR_INMEDIATO:"PORPAGINM", CONSTANT_ROW_VALUE: 33.75};
var global_codigo_prestamo = '';
var global_email_destino_prestamo;
var global_numero_cuota_por_pagar = 0;
var count_clicks_email_prestamo = 0;

var CONSTANT_ALERT_TYPE_PROX_CUOT = 1;
var CONSTANT_ALERT_TYPE_ANTES_VENC = 2;


var numeroPrestamo = '';
var idPrestamo = '';
var tipoPrestamo = '';
var numeroCliente = '';

console.log( "ready!" );
console.log("jQuery "+ (jQuery ? $().jquery : "NOT") +" loaded");
	
function cargar_informacion_prestamo(IdPrestamo, NumeroPrestamo, NumeroCliente){
	count_clicks_email_prestamo = 0;
	console.log("cargar_informacion_prestamo entro!");
	
	$("#modalLoading").modal('show');
	
	global_codigo_prestamo = IdPrestamo;
	
	$.ajax({
        type : 'POST',
        url  : '<%=sendDataPrestamo%>',
        data: { 
        	<portlet:namespace/>cmd : "CommandConsultaDetallePrestamo",
        	<portlet:namespace/>idPrestamo : IdPrestamo,
        	<portlet:namespace/>nroPrestamo : NumeroPrestamo,
        	<portlet:namespace/>nroCliente: NumeroCliente
        },
        beforeSend : function(data){
        	console.log("beforeSend!!");
        	inicializarCamposDetallePrestamo();
        },
        success : function(data){
        	console.log("success!!");
        	var dataDetallePrestamo = JSON.parse(data);
        	console.log(dataDetallePrestamo);
        	inicializarCamposDetallePrestamo();
        	asignarValoresCamposDetallePrestamo(dataDetallePrestamo);
        	
        	
        	console.log("por implementr scrool!!");
        	console.log("global_numero_cuota_por_pagar "+global_numero_cuota_por_pagar);
        	scrollPago(global_numero_cuota_por_pagar);
        	
        	
        	$("#modalLoading").modal('hide');
        	$("#modalPrestamos").modal('show'); 
        	
        	var diasAntesAlerta = dataDetallePrestamo.detalleprestamo.diasAntesAlerta;
        	
        	if(diasAntesAlerta){
            	$('#inputalertantesprestamo').val(diasAntesAlerta);
            	$('#iptChkPrestamo2').prop('checked', true);
        	}
        	if(dataDetallePrestamo.detalleprestamo.proxCuotaAlerta){
            	$('#iptChkPrestamo1').prop('checked', true);
        	}
        },
        error: function(data) {
           }
	   });
	
	
	
	
	
	$('#sendEmailPrestamo').on("click", function () {
		
		var codigoPrestamo = global_codigo_prestamo;
		var fechaHoy = GetTodayDate();
		$("#destinatarioEmailPrestamo").val(global_email_destino_prestamo);
		$("#asuntoEmailPrestamo").val("Informaci�n sobre Pr�stamo "+codigoPrestamo+" al "+fechaHoy);
		$("#commentPrestamo").val('');
		
		count_clicks_email_prestamo = 0;
		
		
		
		
		
	});
	
	
	
	//TODO madar parametros
	$('#enviar_email_prestamo').on("click", function (event) {
		
		$("#enviar_email_prestamo").attr("disabled", true);
		
		event.preventDefault();
		
		count_clicks_email_prestamo++;
		if (count_clicks_email_prestamo==1){
			var varDestinatarioEmail = $("#destinatarioEmailPrestamo").val();
			var varAsuntoEmail = $("#asuntoEmailPrestamo").val();
			var varCommentEmail = $("#commentPrestamo").val();
			
			
			
			$.ajax({
		        type : 'POST',
		        url  : '<%=sendDataPrestamo%>',
		        data: { 
		        	<portlet:namespace/>cmd : "CommandEnviarEmailPrestamo",
		        	<portlet:namespace/>destinatarioEmail : varDestinatarioEmail,
		        	<portlet:namespace/>asuntoEmail : varAsuntoEmail,
		        	<portlet:namespace/>comentarioEmail : varCommentEmail
		        },
		        beforeSend : function(data){
		        	
		        },
		        success : function(data){
		        	console.log(data);
		        	
		        	$('#enviar_email_prestamo').removeAttr('disabled');
		        	//if (data.respuesta=='ok'){
	        		alert('Correo enviado');
	            	$('#modalSendEmailPrestamo').modal('hide');
		        	//}
		        	
		        },
		        error : function(data){
		        	alert('Problema al enviar correo');
		        }
			   });
		}
		
		
		
		
	});
	
	
	
	
	
	
}


function scrollPago(paramRow){
	
	var valorPosicion = CONSTANTES_PRESTAMO.CONSTANT_ROW_VALUE * (paramRow - 1);
	
	$("#body_calendario_pago_prestamo").animate({ scrollTop: valorPosicion });
}

function GetTodayDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd
	}

	if (mm < 10) {
		mm = '0' + mm
	}

	today = dd + '/' + mm + '/' + yyyy;

	return today;
}

function inicializarCamposDetallePrestamo(){
	$("#detalle_prestamo_descripcion").html('');
	$("#detalle_prestamo_moneda").html('');
	$("#detalle_prestamo_saldo_capital").html('');
	$("#detalle_prestamo_tasa_interes").html('');
	$("#detalle_prestamo_fecha_pago").html('');
	$("#detalle_prestamo_monto_prestamo").html('');
	$("#detalle_prestamo_fecha_desembolso").html('');
	$("#detalle_prestamo_fecha_vencimiento").html('');
	$("#detalle_prestamo_frecuencia_capital_interes").html('');
	$("#detalle_prestamo_interes_acumulado").html('');
	$("#body_calendario_pago_prestamo").html('');
	$('#inputalertantesprestamo').val(2);
	$('#iptChkPrestamo2').prop('checked', false);
	$('#iptChkPrestamo1').prop('checked', false);
}


function asignarValoresCamposDetallePrestamo(prestamo){
	global_email_destino_prestamo = prestamo.detalleprestamo.emailDestino;
	$("#detalle_prestamo_descripcion").text('Pr�stamo '+prestamo.detalleprestamo.despre);
	$("#detalle_prestamo_moneda").text('Moneda: '+prestamo.detalleprestamo.descripcionMonedaFormato);
	$("#detalle_prestamo_saldo_capital").text(prestamo.detalleprestamo.saldoCapitalFormato);
	$("#detalle_prestamo_tasa_interes").text(prestamo.detalleprestamo.tasaInteresFormato+'%');
	$("#detalle_prestamo_fecha_pago").text(prestamo.detalleprestamo.fechaProximoPagoFormato);
	$("#detalle_prestamo_monto_prestamo").text(prestamo.detalleprestamo.montoPrestamoFormato);
	$("#detalle_prestamo_fecha_desembolso").text(prestamo.detalleprestamo.fechaDesembolsoFormato);
	$("#detalle_prestamo_fecha_vencimiento").text(prestamo.detalleprestamo.fechaVencimientoFormato);
	$("#detalle_prestamo_frecuencia_capital_interes").text(prestamo.detalleprestamo.feccai);
	$("#detalle_prestamo_interes_acumulado").text(prestamo.detalleprestamo.interesAcumuladoFormato);
	
	
	var strHtmlCalendarioPago;
	$.each(prestamo.detallecalendariopago, function(index, value) {
		if(value.estadoPagoCalculado==CONSTANTES_PRESTAMO.ESTADO_PAGO_CANCELADO){
			strHtmlCalendarioPago += 
				"<tr class='paid-prestamo'>";
		}
		if(value.estadoPagoCalculado==CONSTANTES_PRESTAMO.ESTADO_PAGO_POR_PAGAR_INMEDIATO){
			strHtmlCalendarioPago += 
				"<tr class='pending-prestamo'>";
				
				
			global_numero_cuota_por_pagar = value.numcuo;
		
		}
		if(value.estadoPagoCalculado==CONSTANTES_PRESTAMO.ESTADO_PAGO_POR_PAGAR){
			strHtmlCalendarioPago += 
				"<tr class='nextPaid-prestamo'>";
		}
		strHtmlCalendarioPago += 
			"<td>" + value.numcuo + " de " + value.totcuo + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+ 
			"<td>" + value.capitalFormato + "</td>" + 
			"<td>" + value.interesFormato + "</td>" +
			"<td>" + value.montoFormato + "</td>" +
			"<td>" + value.fechaPagoFormato + "</td>" + 
			"</tr>";
		});
	
	$("#body_calendario_pago_prestamo").append(strHtmlCalendarioPago);
}
	
	

	jQuery(document).ready(function() {

		$('#modalPrestamos').on('shown', function() {
			console.log("I want this to appear after the modal has opened!");
        });
		
		$("body").on("click",'.btndetalleprestamo', function () {
			numeroPrestamo = $(this).attr('data-nroprestamo');
			idPrestamo = $(this).attr('data-idprestamo');
			tipoPrestamo = $(this).attr('data-tipoprestamo');
			numeroCliente = $(this).attr('data-nrocliente');
			cargar_informacion_prestamo(idPrestamo, numeroPrestamo, numeroCliente);
		});

// 		ALERTAS
		$("body").on("click",'#iptChkPrestamo1', function () {
			alertaAntesVencPrestamo(CONSTANT_ALERT_TYPE_PROX_CUOT);
		});
		
		$("body").on("click",'#iptChkPrestamo2', function () {
			alertaAntesVencPrestamo(CONSTANT_ALERT_TYPE_ANTES_VENC);
		});
		
		$("body").on("click",'.input-number-increment', function () {
			if($('#iptChkPrestamo2').prop('checked')){
				alertaAntesVencPrestamo(CONSTANT_ALERT_TYPE_ANTES_VENC);
			}
		});
		
		$("body").on("click",'.input-number-decrement', function () {
			if($('#iptChkPrestamo2').prop('checked')){
				alertaAntesVencPrestamo(CONSTANT_ALERT_TYPE_ANTES_VENC);
			}
		});
		
		function alertaAntesVencPrestamo(type_alert){
			
			var inputalertantesprestamo = $('#inputalertantesprestamo').val();
			var alertaActivado = '';
			
			if(type_alert == CONSTANT_ALERT_TYPE_ANTES_VENC){
				alertaActivado = $('#iptChkPrestamo2').prop('checked');
			}else if(type_alert == CONSTANT_ALERT_TYPE_PROX_CUOT){
				alertaActivado = $('#iptChkPrestamo1').prop('checked');
			}

			$.ajax({
		        type : 'POST',
		        url  : '<%=sendDataPrestamo%>',
		        data: { 
		        	<portlet:namespace/>cmd : "CommandSetAlertAntesVencerPrestamo",
		        	<portlet:namespace/>inputalertantesprestamo: inputalertantesprestamo,
		        	<portlet:namespace/>alertaActivado: alertaActivado,
		        	<portlet:namespace/>type_alert: type_alert
		        },
		        success : function(data){
		        	console.info(data);
		        },
		        error: function(data) {
		           }
			   });
		}

	});
</script>