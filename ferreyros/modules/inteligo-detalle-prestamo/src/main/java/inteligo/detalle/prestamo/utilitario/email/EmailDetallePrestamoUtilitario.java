package inteligo.detalle.prestamo.utilitario.email;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;

public class EmailDetallePrestamoUtilitario {

	
	public static void sendMailWithAttachment(String fromAddress, String[] toAddress, String subject, String body, byte[] bytesFile, String fileNameAttached) throws Exception {
		

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String fechaSistemaArchivo = dateFormat.format(date);

		File file = File.createTempFile("temporal" + fechaSistemaArchivo, ".pdf");
		FileOutputStream stream = new FileOutputStream(file);

		stream.write(bytesFile);
		stream.close();
		

		try {
			MailMessage mailMessage = new MailMessage();
			
			
			InternetAddress[] toAdressArray = new InternetAddress[toAddress.length];
			
			int contador = 0;
			for (String strToAddress : toAddress) {
				toAdressArray[contador] = new InternetAddress(strToAddress);
				contador++;
			}
			
			
			mailMessage.setTo(toAdressArray);
			mailMessage.setFrom(new InternetAddress(fromAddress));
			mailMessage.setSubject(subject);
			mailMessage.setBody(body);
			mailMessage.addFileAttachment(file, fileNameAttached);
			MailServiceUtil.sendEmail(mailMessage);
		} catch (AddressException e) {
			e.printStackTrace();
		} 
		
	}

	
	
}
