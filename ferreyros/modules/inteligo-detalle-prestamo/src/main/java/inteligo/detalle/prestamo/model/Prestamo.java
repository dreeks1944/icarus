package inteligo.detalle.prestamo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Prestamo implements Serializable{

	private static final long serialVersionUID = -2808018888473416365L;

	@SerializedName("detalleprestamo")
	private DetallePrestamo detallePrestamo;
	
	@SerializedName("detallecalendariopago")
	private List<CalendarioPagoPrestamo> listCalendarioPrestamo = new ArrayList<CalendarioPagoPrestamo>();

	public DetallePrestamo getDetallePrestamo() {
		return detallePrestamo;
	}

	public void setDetallePrestamo(DetallePrestamo detallePrestamo) {
		this.detallePrestamo = detallePrestamo;
	}

	public List<CalendarioPagoPrestamo> getListCalendarioPrestamo() {
		return listCalendarioPrestamo;
	}

	public void setListCalendarioPrestamo(List<CalendarioPagoPrestamo> listCalendarioPrestamo) {
		this.listCalendarioPrestamo = listCalendarioPrestamo;
	}

	
	
	
}
