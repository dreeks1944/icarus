package inteligo.detalle.prestamo.utilitario.pdf;

import java.io.IOException;
import java.net.MalformedURLException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;


/**
 * @author Vidal Cupe
 * @see Utileria de generacion de reportes pdf
 * ------------------------------------------------------
 * @author Amador Cabellos
 * @see Utileria de generacion de reportes pdf
 * FECHA: 15/05/2012
 * 
 * construirCelda(PdfPTable, Phrase, float, float, float, float, float, boolean, int, int, int, Color)
 * construirCeldaBorde(PdfPTable, Paragraph, float, float, float, float, float, boolean, int, int, int, Color)
 * construirCeldaImagen(PdfPTable, String, int, int, int, Color)
 * generaCabeceraCuadratura(PdfPTable, String, String, String, String, String)
 * generaCabeceraPrelacion(PdfPTable, String, String, String, String, String, String)
 * generaCabeceraVentaFacturaAcreedor(PdfPTable, String, String, String, String, String)
 * generaCabeceraVentaFacturaNormal(PdfPTable, String, String, String, String, String)
 * generaCabeceraVentaFacturaSuspenso(PdfPTable, String, String, String, String, String)
 * generaCabeceraVentaPagoSaga(PdfPTable, String, String, String)
 * generateAsientoContable(PdfPTable, String, List<AsientoContableReporte>, List<String>)
 * generaValidacionPago188(PdfPTable, String, String, String, String, String)
 * ------------------------------------------------------
 */


public class PdfUtil {
				
		public static void construirCelda(PdfPTable table, Phrase phrase, float top,
				float bottom, float right, float left, float padding, boolean wrap,
				int horizontal, int vertical, int colspan, BaseColor color) {
			PdfPCell cell = new PdfPCell(phrase);
			cell.setBorderWidthBottom(bottom);
			cell.setBorderWidthLeft(left);
			cell.setBorderWidthRight(right);
			cell.setBorderWidthTop(top);
			cell.setPadding(padding);
			cell.setHorizontalAlignment(horizontal);
			cell.setVerticalAlignment(vertical);
			cell.setNoWrap(wrap);
			if (colspan != 0)
				cell.setColspan(colspan);
			cell.setBackgroundColor(color);			
			table.addCell(cell);
		}
		
		
		public static void construirCeldaBorde(PdfPTable table, Paragraph paragraph, float top,
				float bottom, float right, float left, float padding, boolean wrap,
				int horizontal, int vertical, int colspan, BaseColor color) {
			PdfPCell cell = new PdfPCell(paragraph);
			cell.setBorderWidthBottom(bottom);
			cell.setBorderWidthLeft(left);
			cell.setBorderWidthRight(right);
			cell.setBorderWidthTop(top);
			cell.setPadding(padding);
			cell.setHorizontalAlignment(horizontal);
			cell.setVerticalAlignment(vertical);			
			if (colspan != 0)
				cell.setColspan(colspan);
			cell.setBackgroundColor(color);			
			table.addCell(cell);
		}
	
		
		
		public static void construirCeldaImagen(PdfPTable table, String path,int horizontal, int vertical, int colspan,BaseColor color){	             
			PdfPCell cell = new PdfPCell();
			Image image;
			try {
				image = Image.getInstance(path);
				cell.setBorder(0);
				cell.setImage(image);
				cell.setHorizontalAlignment(horizontal);
				cell.setVerticalAlignment(vertical);
				if (colspan != 0)
					cell.setColspan(colspan);
				cell.setBackgroundColor(color);
				table.addCell(cell);
			} catch (BadElementException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
		public static void construirLineaVacia(PdfPTable table){	             
			Font fontTituloDefault = new Font(FontFamily.HELVETICA, 5, Font.BOLD,BaseColor.BLACK);
			Paragraph paragraph = new Paragraph("",fontTituloDefault);
			PdfUtil.construirCelda(table, paragraph,0.0f, 0.0f,0.0f,0.0f, 4f, false,
					Element.ALIGN_CENTER, Element.ALIGN_CENTER, 4, BaseColor.WHITE);
			paragraph = new Paragraph("",fontTituloDefault);
			PdfUtil.construirCelda(table, paragraph,0.0f, 0.0f,0.0f,0.0f, 4f, false,
					Element.ALIGN_CENTER, Element.ALIGN_CENTER, 4, BaseColor.WHITE);
			
		}
		
		
		
		
	
		
		
		
		
		
		
}
