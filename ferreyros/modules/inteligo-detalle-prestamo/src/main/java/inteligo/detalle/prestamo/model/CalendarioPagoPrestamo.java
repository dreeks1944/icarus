package inteligo.detalle.prestamo.model;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

public class CalendarioPagoPrestamo {

	@SerializedName("totcuo")
	private Integer totalCuota;
	
	@SerializedName("numcuo")
	private Integer numeroCuota;
	
	@SerializedName("cappre")
	private BigDecimal capital;
	
	@SerializedName("intpre")
	private BigDecimal interes;
	
	@SerializedName("monto")
	private BigDecimal monto;
	
	@SerializedName("fecpag")
	private String fechaPago;
	
	@SerializedName("estpag")
	private String estadoPago;
	
	
	//inicio formatos
	private String capitalFormato;
	private String interesFormato;
	private String montoFormato;
	private String fechaPagoFormato;
	private String estadoPagoCalculado;
	//fin formatos
	
	
	public Integer getTotalCuota() {
		return totalCuota;
	}
	public void setTotalCuota(Integer totalCuota) {
		this.totalCuota = totalCuota;
	}
	public Integer getNumeroCuota() {
		return numeroCuota;
	}
	public void setNumeroCuota(Integer numeroCuota) {
		this.numeroCuota = numeroCuota;
	}
	public BigDecimal getCapital() {
		return capital;
	}
	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}
	public BigDecimal getInteres() {
		return interes;
	}
	public void setInteres(BigDecimal interes) {
		this.interes = interes;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getEstadoPago() {
		return estadoPago;
	}
	public void setEstadoPago(String estadoPago) {
		this.estadoPago = estadoPago;
	}
	public String getCapitalFormato() {
		return capitalFormato;
	}
	public void setCapitalFormato(String capitalFormato) {
		this.capitalFormato = capitalFormato;
	}
	public String getInteresFormato() {
		return interesFormato;
	}
	public void setInteresFormato(String interesFormato) {
		this.interesFormato = interesFormato;
	}
	public String getMontoFormato() {
		return montoFormato;
	}
	public void setMontoFormato(String montoFormato) {
		this.montoFormato = montoFormato;
	}
	public String getFechaPagoFormato() {
		return fechaPagoFormato;
	}
	public void setFechaPagoFormato(String fechaPagoFormato) {
		this.fechaPagoFormato = fechaPagoFormato;
	}
	public String getEstadoPagoCalculado() {
		return estadoPagoCalculado;
	}
	public void setEstadoPagoCalculado(String estadoPagoCalculado) {
		this.estadoPagoCalculado = estadoPagoCalculado;
	}
	
	
	
	
	
	
	
}
