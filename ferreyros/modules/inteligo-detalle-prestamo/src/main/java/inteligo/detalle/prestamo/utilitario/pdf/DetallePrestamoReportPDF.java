package inteligo.detalle.prestamo.utilitario.pdf;

import java.io.ByteArrayOutputStream;
import java.util.List;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import inteligo.detalle.prestamo.model.CalendarioPagoPrestamo;
import inteligo.detalle.prestamo.model.DetallePrestamo;

public class DetallePrestamoReportPDF {
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD, BaseColor.BLUE);
    private static Font subTitleFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.DARK_GRAY);
   // private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font fontHeaderDetail = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLUE);
    private static Font fontBodyDetail = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.DARK_GRAY);
	
	public byte[] makePrestamoPagosReportPDF(String contextPath, List<CalendarioPagoPrestamo> listPagos,
			DetallePrestamo detallePrestamo){
		
		try{
			//Creaci�n de la instancia del reporte
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, baos);
			//A�adiendo header y footer del reporte
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			event.setSource(contextPath);
			writer.setPageEvent(event);
			document.open();
			//A�adiendo meta data
			addMetaData(document);
			//A�adiendo titulo del reporte
			addTitlePage(document, detallePrestamo);
			//A�adiento detalle del reporte
			addDetailReport(document, detallePrestamo);
			//A�adiendo cuerpo del documento
			addContent(document,listPagos);
			document.close();
			return baos.toByteArray();
		}catch(DocumentException e){
			return null;
		}
		
	}
	
	private static void addMetaData(Document document) {
        document.addTitle("Pagos Pr�stamo");
        document.addSubject("Pr�stamo");
        document.addKeywords("Inteligo, Bank, Visa, Signature");
        document.addAuthor("Inteligo Bank");
        document.addCreator("Arturo D.");
    }
	
	private static void addTitlePage(Document document, DetallePrestamo detallePrestamo) throws DocumentException {
        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("Pr�stamo " +detallePrestamo.getDescripcionPrestamo(), titleFont));
        addEmptyLine(preface, 1);
        //preface.add(new Paragraph("Pr�stamo: "+detallePrestamo.getDescripcionPrestamo(), subTitleFont));
        preface.add(new Paragraph("Moneda: "+detallePrestamo.getDescripcionMoneda(), subTitleFont));
        addEmptyLine(preface, 1);
        document.add(preface);
    }
	
	private static void addDetailReport(Document document, DetallePrestamo detallePrestamo) throws DocumentException {
		
		PdfPTable table = null;
		
		
		table = new PdfPTable(4);
		 
		 
		PdfPCell cell;
		
		//First block
		cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Saldo Capital", fontHeaderDetail)));
        
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getSaldoCapitalFormato(), fontBodyDetail)));
        
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setUseBorderPadding(true);
		cell.setBorderWidthLeft(0.5f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.25f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Tasa inter�s", fontHeaderDetail)));
        
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getTasaInteresFormato() + "%", fontBodyDetail)));
       
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.25f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        
        
    	cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Fecha pr�ximo pago", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getFechaProximoPagoFormato(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.25f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        	
        
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Monto del pr�stamo", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getMontoPrestamoFormato(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.5f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        document.add(table);
        
        table = new PdfPTable(4);
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Fecha de desembolso", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getFechaDesembolsoFormato(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.5f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Fecha vencimiento", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getFechaVencimientoFormato(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.5f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Frec. Capital/Inter�s", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getFrecuenciaCapitalInteres(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.5f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        cell = new PdfPCell();
        cell.addElement(new Paragraph(new Phrase("Intereses Acumulados", fontHeaderDetail)));
        cell.addElement(new Paragraph(new Phrase(detallePrestamo.getInteresAcumuladoFormato(), fontBodyDetail)));
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0.25f);
		cell.setBorderColorLeft(BaseColor.BLUE);
		cell.setBorderWidthTop(0.5f);
		cell.setBorderColorTop(BaseColor.BLUE);
		cell.setBorderWidthBottom(0.25f);
		cell.setBorderColorBottom(BaseColor.BLUE);
		cell.setBorderWidthRight(0.5f);
		cell.setBorderColorRight(BaseColor.BLUE);
        table.addCell(cell);
        
        
        document.add(table);
        
        addEmptyLine(document, 2);
    }
	
	private static void addContent(Document document, List<CalendarioPagoPrestamo> listPago) throws DocumentException {

        // add a table
        createTable(document, listPago);

    }
	
	private static void createTable(Document document, List<CalendarioPagoPrestamo> listPago) throws DocumentException {

        Font font = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.WHITE);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		PdfPCell cell = new PdfPCell(new Phrase("N�", font));
		cell.setBackgroundColor(BaseColor.BLUE);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Capital", font));
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Interes", font));
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Monto", font));
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Fecha", font));
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
        
        for(CalendarioPagoPrestamo pago : listPago){
        	
        	cell = new PdfPCell(new Phrase(pago.getNumeroCuota()+ " de "+pago.getTotalCuota()));
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(cell);
        	
        	cell = new PdfPCell(new Phrase(pago.getCapitalFormato()));
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        table.addCell(cell);
	        
	        
	        cell = new PdfPCell(new Phrase(pago.getInteresFormato()));
    		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(new Phrase(pago.getMontoFormato()));
    		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        table.addCell(cell);
	        
	        cell = new PdfPCell(new Phrase(pago.getFechaPagoFormato()));
    		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        table.addCell(cell);
        }

        document.add(table);

    }
	
	private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
	
	private static void addEmptyLine(Document document, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
        	document.add(new Paragraph(" "));
        }
    }

}
