package inteligo.detalle.prestamo.constants;

/**
 * @author USUARIO
 */
public class InteligoDetallePrestamoPortletKeys {

	public static final String Inteligo = "Inteligo";
	public static final String InteligoDetallePrestamo = "InteligoDetallePrestamo";
	public static final String CommandConsultaDetallePrestamo = "CommandConsultaDetallePrestamo";
	public static final String CommandExportPrestamoExcel = "CommandExportPrestamoExcel";
	public static final String CommandExportPrestamoPdf = "CommandExportPrestamoPdf";
	public static final String CommandEnviarEmailPrestamo = "CommandEnviarEmailPrestamo";
	public static final String CommandSetAlertAntesVencerPrestamo = "CommandSetAlertAntesVencerPrestamo";
	public static final String CommandSetAlertProxCuotPrestamo = "CommandSetAlertProxCuotPrestamo";
	public static final String fromAddressConfiguracionEmail = "alertas2@inteligogroup.com";
	//public static final String fromAddressConfiguracionEmail = "amadorcabellos@gmail.com";
	
	public static final String UrlRestPrestamo = "http://localhost:8083/detalleprestamo";
//	public static final String UrlRestPrestamo = "http://172.18.11.88:8085/APIItlgOmnicanal/api/SMCPE1800220";
	
	public static final String formatoFechaLarga = "EEE, dd MMM yyyy";
	public static final String formatoFechaCorta = "dd MMM yyyy";
	public static final String formatoFechaMuyCorta = "dd-MMM-yy";
	
	public static final String estadoPagoPagado = "0";
	public static final String estadoPagoPendiente = "1";
	
	
	public static final String estadoPagoCancelado = "CANC";
	public static final String estadoPagoPorPagar = "PORPAG";
	public static final String estadoPagoPorPagarInmediato = "PORPAGINM";
	
}