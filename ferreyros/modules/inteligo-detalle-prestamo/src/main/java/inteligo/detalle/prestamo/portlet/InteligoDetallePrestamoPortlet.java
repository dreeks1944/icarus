package inteligo.detalle.prestamo.portlet;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inteligo.service.utils.excel.ExcelPrestamo;
import com.inteligo.service.utils.util.Utilitario;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import inteligo.detalle.prestamo.constants.InteligoDetallePrestamoPortletKeys;
import inteligo.detalle.prestamo.model.AlertaVencimientoDTO;
import inteligo.detalle.prestamo.model.CalendarioPagoPrestamo;
import inteligo.detalle.prestamo.model.Prestamo;
import inteligo.detalle.prestamo.utilitario.email.EmailDetallePrestamoUtilitario;
import inteligo.detalle.prestamo.utilitario.pdf.DetallePrestamoReportPDF;

/**
 * @author USUARIO
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=" + InteligoDetallePrestamoPortletKeys.Inteligo,
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=inteligo-detalle-prestamo Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + InteligoDetallePrestamoPortletKeys.InteligoDetallePrestamo,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class InteligoDetallePrestamoPortlet extends MVCPortlet {

	private static final String ALERT_ANTES_VENC = "ALERT_ANTES_VENC";
	private static final String ALERT_PROX_CUOT = "ALERT_PROX_CUOT";
	private static final String CONSTANT_ALERT_TYPE_PROX_CUOT = "1";
	private static final String CONSTANT_ALERT_TYPE_ANTES_VENC = "2";
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		
		PortletSession session = resourceRequest.getPortletSession();
		String cmd = ParamUtil.getString(resourceRequest, "cmd");
		String idPrestamo = ParamUtil.getString(resourceRequest, "idPrestamo");
		String nroPrestamo = ParamUtil.getString(resourceRequest, "nroPrestamo");
		String nroCliente = ParamUtil.getString(resourceRequest, "nroCliente");
		ThemeDisplay  themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		System.out.println("cmd: " + cmd);
		System.out.println("idPrestamo: " + idPrestamo);
		System.out.println("nroPrestamo: " + nroPrestamo);
		System.out.println("nroCliente: " + nroCliente);

		Gson gson = new Gson();
		
		if (cmd.equals(InteligoDetallePrestamoPortletKeys.CommandConsultaDetallePrestamo)) {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add("idclie", nroCliente);
			headers.add("idpre", idPrestamo);
			headers.add("codpre", nroPrestamo);
			
			
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
			String detallePrestamoResourceUrl = InteligoDetallePrestamoPortletKeys.UrlRestPrestamo;
			
			
			ResponseEntity<String> responseDetallePrestamo = restTemplate.exchange(detallePrestamoResourceUrl,HttpMethod.GET, new HttpEntity<String>(headers), String.class);
			//ResponseEntity<String> responseDetallePrestamo = restTemplate.getForEntity(detallePrestamoResourceUrl, String.class);
			
			String mockupDetallePrestamo = responseDetallePrestamo.getBody();
			// 1. JSON to Java object, read it from a mockup.
			Prestamo detallePrestamo = gson.fromJson(mockupDetallePrestamo, Prestamo.class);
			detallePrestamo.getDetallePrestamo().setEmailDestino(themeDisplay.getUser().getEmailAddress());
			obtenerFormatoPagosPrestamo(detallePrestamo);
			
			//ACTIVAR LA ALERTA GUARDADA
			String jsonValorAlertVenc = (String)themeDisplay.getUser().getExpandoBridge().getAttribute(ALERT_ANTES_VENC);
			AlertaVencimientoDTO alertAntesVencGuardada = getAlertaGuardada(jsonValorAlertVenc, nroPrestamo);

			String jsonValorAlertProxCuot = (String)themeDisplay.getUser().getExpandoBridge().getAttribute(ALERT_PROX_CUOT);
			AlertaVencimientoDTO alertProxCuotGuardada = getAlertaGuardada(jsonValorAlertProxCuot, nroPrestamo);
			
			detallePrestamo.getDetallePrestamo().setDiasAntesAlerta(alertAntesVencGuardada.getAlertantes());
			detallePrestamo.getDetallePrestamo().setProxCuotaAlerta(alertProxCuotGuardada.getAlertantes());

			// 2. Java object to JSON, and assign to a String
			String detallePrestamoJsonInString = gson.toJson(detallePrestamo);
			session.setAttribute("responseObjectDetallePrestamo", detallePrestamo, PortletSession.PORTLET_SCOPE);
			session.setAttribute("nroPrestamoDetallePrestamo", nroPrestamo, PortletSession.PORTLET_SCOPE);
			resourceResponse.getWriter().print(detallePrestamoJsonInString);
			
		}
		
		if (cmd.equals(InteligoDetallePrestamoPortletKeys.CommandExportPrestamoExcel)) {

			Prestamo responseObjectDetallePrestamo =  (Prestamo)session.getAttribute("responseObjectDetallePrestamo",PortletSession.PORTLET_SCOPE);
			if (responseObjectDetallePrestamo!=null){
				
				LinkedHashMap<String, Object[]> dataTitulo = null;
				LinkedHashMap<String, Object[]> dataDetalle = null;
				LinkedHashMap<String, Object[]> dataCabecera = null;
				dataTitulo = new LinkedHashMap<String, Object[]>();
				dataTitulo.put("1", new Object[] { "Pr�stamo " + responseObjectDetallePrestamo.getDetallePrestamo().getDescripcionPrestamo() });
				dataCabecera = new LinkedHashMap<String, Object[]>();
				//dataCabecera.put("1", new Object[] { "Pr�stamo: "+ responseObjectDetallePrestamo.getDetallePrestamo().getDescripcionPrestamo()});
				dataCabecera.put("1", new Object[] { "Moneda:" + responseObjectDetallePrestamo.getDetallePrestamo().getDescripcionMoneda()});
				dataCabecera.put("2", new Object[] { "Saldo Capital", "Tasa inter�s" , "Fecha pr�ximo pago", "Monto del pr�stamo"});
				dataCabecera.put("3", new Object[] { responseObjectDetallePrestamo.getDetallePrestamo().getSaldoCapitalFormato(), responseObjectDetallePrestamo.getDetallePrestamo().getTasaInteresFormato() + "%" , responseObjectDetallePrestamo.getDetallePrestamo().getFechaProximoPagoFormato(), responseObjectDetallePrestamo.getDetallePrestamo().getMontoPrestamoFormato()});
				dataCabecera.put("4", new Object[] { "Fecha de desembolso", "Fecha vencimiento", "Frec. Capital/Inter�s", "Intereses Acumulados"});
				dataCabecera.put("5", new Object[] {  responseObjectDetallePrestamo.getDetallePrestamo().getFechaDesembolsoFormato(), responseObjectDetallePrestamo.getDetallePrestamo().getFechaVencimientoFormato(), responseObjectDetallePrestamo.getDetallePrestamo().getFrecuenciaCapitalInteres(), responseObjectDetallePrestamo.getDetallePrestamo().getInteresAcumuladoFormato()});
				dataDetalle = new LinkedHashMap<String, Object[]>();
				dataDetalle.put("1", new Object[] { "N�", "Capital", "Inter�s", "Monto", "Fecha" });
				int contador = 2;
				if (!responseObjectDetallePrestamo.getListCalendarioPrestamo().isEmpty()) {
					for (CalendarioPagoPrestamo calendarioPago : responseObjectDetallePrestamo.getListCalendarioPrestamo()) {
						dataDetalle.put(String.valueOf(contador),
								new Object[] { calendarioPago.getNumeroCuota()+" de "+calendarioPago.getTotalCuota(), calendarioPago.getCapitalFormato(),
										calendarioPago.getInteresFormato(), calendarioPago.getMontoFormato(), calendarioPago.getFechaPagoFormato() });
						contador++;
					}
				}
				ExcelPrestamo.generarHSSFWorkbook(dataTitulo, dataCabecera, dataDetalle);
				resourceResponse.setContentType("application/vnd.ms-excel");
				resourceResponse.addProperty("Content-Disposition", "attachment; filename=" + "Pagos Pr�stamo.xls");
				ExcelPrestamo.setHSSFWorkbook(resourceResponse.getPortletOutputStream());
			}
			else{
				throw new PortletException("Error session terminada");
			}
		}
		
		if (cmd.equals(InteligoDetallePrestamoPortletKeys.CommandExportPrestamoPdf)) {
			
			Prestamo responseObjectDetallePrestamo =  (Prestamo)session.getAttribute("responseObjectDetallePrestamo",PortletSession.PORTLET_SCOPE);
			if (responseObjectDetallePrestamo!=null){
				byte[] bytePdfCash = new DetallePrestamoReportPDF().makePrestamoPagosReportPDF(themeDisplay.getPathThemeImages(), 
						responseObjectDetallePrestamo.getListCalendarioPrestamo(), responseObjectDetallePrestamo.getDetallePrestamo());
				if (bytePdfCash != null) {
					System.out.println("entra");
					resourceResponse.setProperty("Expires", "0");
					resourceResponse.setProperty("Pragma", "public");
					resourceResponse.setContentType("application/pdf");
					resourceResponse.setProperty(HttpHeaders.CONTENT_DISPOSITION,
							"filename=\"" + "Pagos Prestamo.pdf" + "\"");
					java.io.OutputStream outputStream = resourceResponse.getPortletOutputStream();
					outputStream.write(bytePdfCash);
					outputStream.flush();
					outputStream.close();
					resourceResponse.setContentType("application/json");
					resourceResponse.setCharacterEncoding("UTF-8");
					resourceResponse.getWriter().write("success....");
				}
			}
			else{
				throw new PortletException("Error session terminada");
			}
		}
		
		if (cmd.equals(InteligoDetallePrestamoPortletKeys.CommandEnviarEmailPrestamo)) {
			
			String destinatarioEmail = ParamUtil.getString(resourceRequest, "destinatarioEmail");
			String asuntoEmail = ParamUtil.getString(resourceRequest, "asuntoEmail");
			String comentarioEmail = ParamUtil.getString(resourceRequest, "comentarioEmail");
			
			String toAddress = destinatarioEmail;
			String subject =asuntoEmail;
			String body = comentarioEmail;
			String fileNameAttached = "Pagos Prestamo.pdf";
			String fromAddress = InteligoDetallePrestamoPortletKeys.fromAddressConfiguracionEmail;
			
			Prestamo responseObjectDetallePrestamo =  (Prestamo)session.getAttribute("responseObjectDetallePrestamo",PortletSession.PORTLET_SCOPE);
			if (responseObjectDetallePrestamo!=null){
				byte[] bytePdfCash = new DetallePrestamoReportPDF().makePrestamoPagosReportPDF(themeDisplay.getPathThemeImages(), 
						responseObjectDetallePrestamo.getListCalendarioPrestamo(), responseObjectDetallePrestamo.getDetallePrestamo());
				
				if (bytePdfCash != null) {

					
					try {
						EmailDetallePrestamoUtilitario.sendMailWithAttachment(fromAddress, toAddress.split(";"), subject, body, bytePdfCash,
								fileNameAttached);
						JSONObject respJSON = JSONFactoryUtil.createJSONObject();
						respJSON.put("respuesta", "ok");
						resourceResponse.getWriter().print(respJSON.toString());
					} catch (Exception e) {
						throw new PortletException("Error envio email");
					}

				}
				
			}
			else{
				throw new PortletException("Error session terminada");
			}
			
		} else if (cmd.equals(InteligoDetallePrestamoPortletKeys.CommandSetAlertAntesVencerPrestamo)) {

			int inputalertantesprestamo = ParamUtil.getInteger(resourceRequest, "inputalertantesprestamo");
			boolean alertaActivado = ParamUtil.getBoolean(resourceRequest, "alertaActivado");
			String nroPrestamoSession =  (String)session.getAttribute("nroPrestamoDetallePrestamo",PortletSession.PORTLET_SCOPE);
			String type_alert = ParamUtil.getString(resourceRequest, "type_alert");
			
			if(type_alert.equals(CONSTANT_ALERT_TYPE_ANTES_VENC)){
				
				String jsonValorAlertVenc = (String)themeDisplay.getUser().getExpandoBridge().getAttribute(ALERT_ANTES_VENC);

				List<AlertaVencimientoDTO> alertAntesVencDTO = new ArrayList<AlertaVencimientoDTO>();
				
				if(Validator.isNotNull(jsonValorAlertVenc)){
					Type listType = new TypeToken<ArrayList<AlertaVencimientoDTO>>(){}.getType();
					alertAntesVencDTO = gson.fromJson(jsonValorAlertVenc, listType);
				}
				
				boolean existeAlertaDelPrestamo = Boolean.FALSE;
				
				for (int i = 0; i < alertAntesVencDTO.size(); i++) {
					
					AlertaVencimientoDTO alerta = alertAntesVencDTO.get(i);

					if(alerta.getCodpre().equals(nroPrestamoSession)){
						
						existeAlertaDelPrestamo = Boolean.TRUE;
						
						if(!alertaActivado){
							alertAntesVencDTO.remove(i);
						}else{
							alerta.setAlertantes(inputalertantesprestamo);
						}
							
					}
				}
				
				if(!existeAlertaDelPrestamo){
					alertAntesVencDTO.add(new AlertaVencimientoDTO(nroPrestamoSession, inputalertantesprestamo, Boolean.FALSE));
				}
				
				jsonValorAlertVenc = gson.toJson(alertAntesVencDTO);
				themeDisplay.getUser().getExpandoBridge().setAttribute(ALERT_ANTES_VENC, jsonValorAlertVenc);
				
			}else if(type_alert.equals(CONSTANT_ALERT_TYPE_PROX_CUOT)){

				String jsonValorAlertVenc = (String)themeDisplay.getUser().getExpandoBridge().getAttribute(ALERT_PROX_CUOT);

				List<AlertaVencimientoDTO> alertAntesVencDTO = new ArrayList<AlertaVencimientoDTO>();
				
				if(Validator.isNotNull(jsonValorAlertVenc)){
					Type listType = new TypeToken<ArrayList<AlertaVencimientoDTO>>(){}.getType();
					alertAntesVencDTO = gson.fromJson(jsonValorAlertVenc, listType);
				}
				
				boolean existeAlertaDelPrestamo = Boolean.FALSE;
				
				for (int i = 0; i < alertAntesVencDTO.size(); i++) {
					
					AlertaVencimientoDTO alerta = alertAntesVencDTO.get(i);

					if(alerta.getCodpre().equals(nroPrestamoSession)){
						
						existeAlertaDelPrestamo = Boolean.TRUE;
						
						if(!alertaActivado){
							alertAntesVencDTO.remove(i);
						}else{
							alerta.setAlertantes(-1);
						}
							
					}
				}
				
				if(!existeAlertaDelPrestamo){
					alertAntesVencDTO.add(new AlertaVencimientoDTO(nroPrestamoSession, -1, Boolean.FALSE));
				}
				
				jsonValorAlertVenc = gson.toJson(alertAntesVencDTO);
				themeDisplay.getUser().getExpandoBridge().setAttribute(ALERT_PROX_CUOT, jsonValorAlertVenc);
			}
			
			resourceResponse.getWriter().print("success");
			
		} 
	}
	
	private void obtenerFormatoPagosPrestamo(Prestamo prestamo) {
		
		//dar formato a detalle prestamo y calendario de pagos
		prestamo.getDetallePrestamo().setSaldoCapitalFormato(Utilitario.obtenerMontoFormato(prestamo.getDetallePrestamo().getSaldoCapital()));
		prestamo.getDetallePrestamo().setTasaInteresFormato(Utilitario.obtenerMontoFormato(prestamo.getDetallePrestamo().getTasaInteres()));
		prestamo.getDetallePrestamo().setMontoPrestamoFormato(Utilitario.obtenerMontoFormato(prestamo.getDetallePrestamo().getMontoPrestamo()));
		prestamo.getDetallePrestamo().setInteresAcumuladoFormato(Utilitario.obtenerMontoFormato(prestamo.getDetallePrestamo().getInteresAcumulado()));
		prestamo.getDetallePrestamo().setFechaDesembolsoFormato(Utilitario.obtenerFechaPersonalizadoFormato(prestamo.getDetallePrestamo().getFechaDesembolso(),InteligoDetallePrestamoPortletKeys.formatoFechaCorta));
		prestamo.getDetallePrestamo().setFechaProximoPagoFormato(Utilitario.obtenerFechaPersonalizadoFormato(prestamo.getDetallePrestamo().getFechaProximoPago(),InteligoDetallePrestamoPortletKeys.formatoFechaCorta));
		prestamo.getDetallePrestamo().setFechaVencimientoFormato(Utilitario.obtenerFechaPersonalizadoFormato(prestamo.getDetallePrestamo().getFechaVencimiento(),InteligoDetallePrestamoPortletKeys.formatoFechaCorta));
		prestamo.getDetallePrestamo().setDescripcionMonedaFormato(Utilitario.obtenerDescripcionMoneda(prestamo.getDetallePrestamo().getAcronimoMoneda()));
		
		Integer numeroCuotasPorPagar = 0;
		if (!CollectionUtils.isEmpty(prestamo.getListCalendarioPrestamo())){
			for (CalendarioPagoPrestamo pagoprestamo : prestamo.getListCalendarioPrestamo()) {
				pagoprestamo.setCapitalFormato(Utilitario.obtenerMontoFormato(pagoprestamo.getCapital()));
				pagoprestamo.setInteresFormato(Utilitario.obtenerMontoFormato(pagoprestamo.getInteres()));
				pagoprestamo.setMontoFormato(Utilitario.obtenerMontoFormato(pagoprestamo.getMonto()));
				pagoprestamo.setFechaPagoFormato(Utilitario.obtenerFechaPersonalizadoFormato(pagoprestamo.getFechaPago(),InteligoDetallePrestamoPortletKeys.formatoFechaCorta));
				
				if (pagoprestamo.getEstadoPago().equals(InteligoDetallePrestamoPortletKeys.estadoPagoPagado)){
					pagoprestamo.setEstadoPagoCalculado(InteligoDetallePrestamoPortletKeys.estadoPagoCancelado);
				}
				else if (pagoprestamo.getEstadoPago().equals(InteligoDetallePrestamoPortletKeys.estadoPagoPendiente)){
					numeroCuotasPorPagar++;
					if (numeroCuotasPorPagar.equals(1)){
						pagoprestamo.setEstadoPagoCalculado(InteligoDetallePrestamoPortletKeys.estadoPagoPorPagarInmediato);
					}
					else{
						pagoprestamo.setEstadoPagoCalculado(InteligoDetallePrestamoPortletKeys.estadoPagoPorPagar);
					}
				}
				
				
				
			}
		}
		
		//oredenar por fecha de pago
		Collections.sort(prestamo.getListCalendarioPrestamo(), new Comparator<CalendarioPagoPrestamo>(){
			  public int compare(CalendarioPagoPrestamo p1, CalendarioPagoPrestamo p2){
			    return p1.getNumeroCuota().compareTo(p2.getNumeroCuota());
			  }
			});
	}
	
	public AlertaVencimientoDTO getAlertaGuardada(String jsonValorAlert, String nroPrestamo){
		
		Gson gson = new Gson();
		
		List<AlertaVencimientoDTO> alertAntesVencDTO = new ArrayList<AlertaVencimientoDTO>();
		
		if(Validator.isNotNull(jsonValorAlert)){
			Type listType = new TypeToken<ArrayList<AlertaVencimientoDTO>>(){}.getType();
			alertAntesVencDTO = gson.fromJson(jsonValorAlert, listType);
		}
		
		AlertaVencimientoDTO alertaGuardada = new AlertaVencimientoDTO();
		
		for (AlertaVencimientoDTO alertasGuardadas : alertAntesVencDTO) {

			if(alertasGuardadas.getCodpre().equals(nroPrestamo)){
				alertaGuardada = alertasGuardadas;
			}
		}
		
		return alertaGuardada;
	}
	
}