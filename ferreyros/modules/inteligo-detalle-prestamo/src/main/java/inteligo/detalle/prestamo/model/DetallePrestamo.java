package inteligo.detalle.prestamo.model;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

public class DetallePrestamo {

	@SerializedName("despre")
	private String descripcionPrestamo;
	
	@SerializedName("desmon")
	private String descripcionMoneda;
	
	@SerializedName("acrmon")
	private String acronimoMoneda;
	
	@SerializedName("simbol")
	private String simboloMoneda;
	
	@SerializedName("salcap")
	private BigDecimal saldoCapital;
	
	@SerializedName("tasint")
	private BigDecimal tasaInteres;
	
	@SerializedName("fecpro")
	private String fechaProximoPago;
	
	@SerializedName("tippre")
	private String tipoPrestamo;
	
	@SerializedName("monpre")
	private BigDecimal montoPrestamo;
	
	@SerializedName("fecdes")
	private String fechaDesembolso;
	
	@SerializedName("fecven")
	private String fechaVencimiento;
	
	@SerializedName("feccai")
	private String frecuenciaCapitalInteres;
	
	@SerializedName("intacu")
	private BigDecimal interesAcumulado;
	
	
	//inicio formatos
	private String saldoCapitalFormato;
	private String tasaInteresFormato;
	private String montoPrestamoFormato;
	private String interesAcumuladoFormato;
	private String fechaProximoPagoFormato;
	private String fechaDesembolsoFormato;
	private String fechaVencimientoFormato;
	
	private String descripcionMonedaFormato;
	//fin formatos
	

	private String emailDestino;

//	Atributos para Alertas
	private int diasAntesAlerta;
	private int proxCuotaAlerta;
	
	public String getDescripcionPrestamo() {
		return descripcionPrestamo;
	}
	public void setDescripcionPrestamo(String descripcionPrestamo) {
		this.descripcionPrestamo = descripcionPrestamo;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public BigDecimal getTasaInteres() {
		return tasaInteres;
	}
	public void setTasaInteres(BigDecimal tasaInteres) {
		this.tasaInteres = tasaInteres;
	}
	public String getFechaProximoPago() {
		return fechaProximoPago;
	}
	public void setFechaProximoPago(String fechaProximoPago) {
		this.fechaProximoPago = fechaProximoPago;
	}
	public BigDecimal getMontoPrestamo() {
		return montoPrestamo;
	}
	public void setMontoPrestamo(BigDecimal montoPrestamo) {
		this.montoPrestamo = montoPrestamo;
	}
	public String getFechaDesembolso() {
		return fechaDesembolso;
	}
	public void setFechaDesembolso(String fechaDesembolso) {
		this.fechaDesembolso = fechaDesembolso;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
	public String getFrecuenciaCapitalInteres() {
		return frecuenciaCapitalInteres;
	}
	public void setFrecuenciaCapitalInteres(String frecuenciaCapitalInteres) {
		this.frecuenciaCapitalInteres = frecuenciaCapitalInteres;
	}
	public BigDecimal getInteresAcumulado() {
		return interesAcumulado;
	}
	public void setInteresAcumulado(BigDecimal interesAcumulado) {
		this.interesAcumulado = interesAcumulado;
	}
	public String getAcronimoMoneda() {
		return acronimoMoneda;
	}
	public void setAcronimoMoneda(String acronimoMoneda) {
		this.acronimoMoneda = acronimoMoneda;
	}
	public String getSimboloMoneda() {
		return simboloMoneda;
	}
	public void setSimboloMoneda(String simboloMoneda) {
		this.simboloMoneda = simboloMoneda;
	}
	public BigDecimal getSaldoCapital() {
		return saldoCapital;
	}
	public void setSaldoCapital(BigDecimal saldoCapital) {
		this.saldoCapital = saldoCapital;
	}
	public String getSaldoCapitalFormato() {
		return saldoCapitalFormato;
	}
	public void setSaldoCapitalFormato(String saldoCapitalFormato) {
		this.saldoCapitalFormato = saldoCapitalFormato;
	}
	public String getTasaInteresFormato() {
		return tasaInteresFormato;
	}
	public void setTasaInteresFormato(String tasaInteresFormato) {
		this.tasaInteresFormato = tasaInteresFormato;
	}
	public String getMontoPrestamoFormato() {
		return montoPrestamoFormato;
	}
	public void setMontoPrestamoFormato(String montoPrestamoFormato) {
		this.montoPrestamoFormato = montoPrestamoFormato;
	}
	public String getInteresAcumuladoFormato() {
		return interesAcumuladoFormato;
	}
	public void setInteresAcumuladoFormato(String interesAcumuladoFormato) {
		this.interesAcumuladoFormato = interesAcumuladoFormato;
	}
	public String getFechaProximoPagoFormato() {
		return fechaProximoPagoFormato;
	}
	public void setFechaProximoPagoFormato(String fechaProximoPagoFormato) {
		this.fechaProximoPagoFormato = fechaProximoPagoFormato;
	}
	public String getFechaDesembolsoFormato() {
		return fechaDesembolsoFormato;
	}
	public void setFechaDesembolsoFormato(String fechaDesembolsoFormato) {
		this.fechaDesembolsoFormato = fechaDesembolsoFormato;
	}
	public String getFechaVencimientoFormato() {
		return fechaVencimientoFormato;
	}
	public void setFechaVencimientoFormato(String fechaVencimientoFormato) {
		this.fechaVencimientoFormato = fechaVencimientoFormato;
	}
	public String getTipoPrestamo() {
		return tipoPrestamo;
	}
	public void setTipoPrestamo(String tipoPrestamo) {
		this.tipoPrestamo = tipoPrestamo;
	}
	public String getDescripcionMonedaFormato() {
		return descripcionMonedaFormato;
	}
	public void setDescripcionMonedaFormato(String descripcionMonedaFormato) {
		this.descripcionMonedaFormato = descripcionMonedaFormato;
	}
	public String getEmailDestino() {
		return emailDestino;
	}
	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}
	public int getDiasAntesAlerta() {
		return diasAntesAlerta;
	}
	public void setDiasAntesAlerta(int diasAntesAlerta) {
		this.diasAntesAlerta = diasAntesAlerta;
	}
	public int getProxCuotaAlerta() {
		return proxCuotaAlerta;
	}
	public void setProxCuotaAlerta(int proxCuotaAlerta) {
		this.proxCuotaAlerta = proxCuotaAlerta;
	}
	
	
	
	
}
