package inteligo.detalle.prestamo.utilitario.pdf;

import java.io.IOException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
	
	private static Font pageFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.DARK_GRAY);
	
	String source;
	
	

    public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void onStartPage(PdfWriter writer, Document document) {
    	Image img;
		try {
			
			img = Image.getInstance(getSource()+"/logo.jpg");
			img.setAlignment(Image.ALIGN_RIGHT);
			img.setAbsolutePosition(30f, 790f);
			img.scalePercent(8, 6);
			document.add(img);
			addEmptyLine(document, 2);
//			ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("que nota"), 30, 800, 0);
//	        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Top Right"), 550, 800, 0);
		} catch (BadElementException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
    }
	
    public void onEndPage(PdfWriter writer, Document document) {
    	
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("P�gina " + writer.getCurrentPageNumber() + "de " +writer.getPageNumber(), pageFont), 280f, 30, 0);
    
    }
    
    private static void addEmptyLine(Document document, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
        	document.add(new Paragraph(" "));
        }
    }

}
